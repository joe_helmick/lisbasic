//UAAMB 		MATCH ("Performed @UAMC", $@UAMC)? "BILLED" : -9999.0
@UAAMB (@UAMC = "Performed @UAMC")

if textin(@UAMC, "Performed @UAMC") then
	say "BILLED"
else
	cancel
end
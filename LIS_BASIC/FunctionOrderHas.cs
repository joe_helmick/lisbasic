﻿namespace LIS_BASIC
{
	public class FunctionOrderHas : SimpleExpression
	{
		private readonly string _test;

		public FunctionOrderHas(Context context, string test)
			: base(context)
		{
			_test = test;
		}

		public override object Value
		{
			get
			{
				if (Context.TestsInOrder.Contains(_test))
					return true;
				return false;
			}
		}
	}
}

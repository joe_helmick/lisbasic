﻿using System.Linq;



namespace LIS_BASIC
{
	public class CancelStatement : SimpleStatement
	{
		private readonly SimpleId _testToCancel;

		public CancelStatement(Context context,
			SimpleId testToCancel)
			: base(context)
		{
			_testToCancel = testToCancel;
		}

		public override void Execute()
		{
			if (Context.Cancels.Contains(_testToCancel.Value) == false)
				Context.Cancels.Add(_testToCancel.Name);
		}
	}
}

﻿
using System;
using System.Reflection;
using System.IO;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;

using GoldParser;

namespace Morozov.Parsing
{
        
    [Serializable()]
    public class SymbolException : System.Exception
    {
        public SymbolException(string message) : base(message)
        {
        }

        public SymbolException(string message,
            Exception inner) : base(message, inner)
        {
        }

        protected SymbolException(SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }

    }

    [Serializable()]
    public class RuleException : System.Exception
    {

        public RuleException(string message) : base(message)
        {
        }

        public RuleException(string message,
                             Exception inner) : base(message, inner)
        {
        }

        protected RuleException(SerializationInfo info,
                                StreamingContext context) : base(info, context)
        {
        }

    }

    enum SymbolConstants : int
    {
        SYMBOL_EOF             =  0, // (EOF)
        SYMBOL_ERROR           =  1, // (Error)
        SYMBOL_COMMENT         =  2, // Comment
        SYMBOL_NEWLINE         =  3, // NewLine
        SYMBOL_WHITESPACE      =  4, // Whitespace
        SYMBOL_DIVDIV          =  5, // '//'
        SYMBOL_MINUS           =  6, // '-'
        SYMBOL_DOLLAR          =  7, // '$'
        SYMBOL_LPAREN          =  8, // '('
        SYMBOL_RPAREN          =  9, // ')'
        SYMBOL_TIMES           = 10, // '*'
        SYMBOL_COMMA           = 11, // ','
        SYMBOL_DIV             = 12, // '/'
        SYMBOL_SEMI            = 13, // ';'
        SYMBOL_CARET           = 14, // '^'
        SYMBOL_PLUS            = 15, // '+'
        SYMBOL_LT              = 16, // '<'
        SYMBOL_LTEQ            = 17, // '<='
        SYMBOL_LTGT            = 18, // '<>'
        SYMBOL_EQ              = 19, // '='
        SYMBOL_EQEQ            = 20, // '=='
        SYMBOL_GT              = 21, // '>'
        SYMBOL_GTEQ            = 22, // '>='
        SYMBOL_AND             = 23, // and
        SYMBOL_CANCEL          = 24, // CANCEL
        SYMBOL_DATELITERAL     = 25, // DateLiteral
        SYMBOL_DATETIMELITERAL = 26, // DateTimeLiteral
        SYMBOL_ELSE            = 27, // ELSE
        SYMBOL_END             = 28, // END
        SYMBOL_HOURS           = 29, // HOURS
        SYMBOL_ID              = 30, // Id
        SYMBOL_IF              = 31, // IF
        SYMBOL_INTLITERAL      = 32, // IntLiteral
        SYMBOL_LEFT            = 33, // LEFT
        SYMBOL_LET             = 34, // LET
        SYMBOL_MID             = 35, // MID
        SYMBOL_MINUTES         = 36, // MINUTES
        SYMBOL_OR              = 37, // or
        SYMBOL_REALLITERAL     = 38, // RealLiteral
        SYMBOL_RIGHT           = 39, // RIGHT
        SYMBOL_SAY             = 40, // SAY
        SYMBOL_STRINGLITERAL   = 41, // StringLiteral
        SYMBOL_TESTNAME        = 42, // TestName
        SYMBOL_TEXTIN          = 43, // TEXTIN
        SYMBOL_THEN            = 44, // THEN
        SYMBOL_ADD             = 45, // <Add>
        SYMBOL_ARG             = 46, // <Arg>
        SYMBOL_ARGS            = 47, // <Args>
        SYMBOL_ARGUMENTS       = 48, // <Arguments>
        SYMBOL_ASSIGNSTMT      = 49, // <AssignStmt>
        SYMBOL_BOOLFUNC        = 50, // <BoolFunc>
        SYMBOL_CANCELSTMT      = 51, // <CancelStmt>
        SYMBOL_COMP            = 52, // <Comp>
        SYMBOL_CONSTANT        = 53, // <Constant>
        SYMBOL_EQU             = 54, // <Equ>
        SYMBOL_EXPR            = 55, // <Expr>
        SYMBOL_FUNCTION        = 56, // <Function>
        SYMBOL_IFSTMT          = 57, // <IfStmt>
        SYMBOL_MULT            = 58, // <Mult>
        SYMBOL_POW             = 59, // <Pow>
        SYMBOL_REALFUNC        = 60, // <RealFunc>
        SYMBOL_SAYSTMT         = 61, // <SayStmt>
        SYMBOL_STMT            = 62, // <Stmt>
        SYMBOL_STMTS           = 63, // <Stmts>
        SYMBOL_STRINGFUNC      = 64, // <StringFunc>
        SYMBOL_TESTNAME2       = 65, // <TestName>
        SYMBOL_UNARY           = 66, // <Unary>
        SYMBOL_VALUE           = 67  // <Value>
    };

    enum RuleConstants : int
    {
        RULE_FUNCTION                                                              =  0, // <Function> ::= <TestName> <Arguments> <Stmts>
        RULE_TESTNAME_TESTNAME                                                     =  1, // <TestName> ::= TestName
        RULE_ARGUMENTS_LPAREN_RPAREN                                               =  2, // <Arguments> ::= '(' <Args> ')'
        RULE_ARGUMENTS_LPAREN_RPAREN2                                              =  3, // <Arguments> ::= '(' ')'
        RULE_ARG_ID_EQ_REALLITERAL                                                 =  4, // <Arg> ::= Id '=' RealLiteral
        RULE_ARG_ID_EQ_STRINGLITERAL                                               =  5, // <Arg> ::= Id '=' StringLiteral
        RULE_ARG_ID_EQ_DATELITERAL                                                 =  6, // <Arg> ::= Id '=' DateLiteral
        RULE_ARG_ID_EQ_DATETIMELITERAL                                             =  7, // <Arg> ::= Id '=' DateTimeLiteral
        RULE_ARG_ID_EQ_INTLITERAL                                                  =  8, // <Arg> ::= Id '=' IntLiteral
        RULE_STMTS                                                                 =  9, // <Stmts> ::= <Stmt> <Stmts>
        RULE_STMTS2                                                                = 10, // <Stmts> ::= <Stmt>
        RULE_ARGS_COMMA                                                            = 11, // <Args> ::= <Arg> ',' <Args>
        RULE_ARGS                                                                  = 12, // <Args> ::= <Arg>
        RULE_CONSTANT_STRINGLITERAL                                                = 13, // <Constant> ::= StringLiteral
        RULE_CONSTANT_REALLITERAL                                                  = 14, // <Constant> ::= RealLiteral
        RULE_CONSTANT_DATELITERAL                                                  = 15, // <Constant> ::= DateLiteral
        RULE_CONSTANT_DATETIMELITERAL                                              = 16, // <Constant> ::= DateTimeLiteral
        RULE_CONSTANT_INTLITERAL                                                   = 17, // <Constant> ::= IntLiteral
        RULE_EXPR_OR                                                               = 18, // <Expr> ::= <Expr> or <Equ>
        RULE_EXPR_AND                                                              = 19, // <Expr> ::= <Expr> and <Equ>
        RULE_EXPR                                                                  = 20, // <Expr> ::= <Equ>
        RULE_EQU_EQEQ                                                              = 21, // <Equ> ::= <Equ> '==' <Comp>
        RULE_EQU_LTGT                                                              = 22, // <Equ> ::= <Equ> '<>' <Comp>
        RULE_EQU                                                                   = 23, // <Equ> ::= <Comp>
        RULE_COMP_GT                                                               = 24, // <Comp> ::= <Comp> '>' <Add>
        RULE_COMP_LT                                                               = 25, // <Comp> ::= <Comp> '<' <Add>
        RULE_COMP_GTEQ                                                             = 26, // <Comp> ::= <Comp> '>=' <Add>
        RULE_COMP_LTEQ                                                             = 27, // <Comp> ::= <Comp> '<=' <Add>
        RULE_COMP                                                                  = 28, // <Comp> ::= <Add>
        RULE_ADD_PLUS                                                              = 29, // <Add> ::= <Add> '+' <Mult>
        RULE_ADD_MINUS                                                             = 30, // <Add> ::= <Add> '-' <Mult>
        RULE_ADD                                                                   = 31, // <Add> ::= <Mult>
        RULE_MULT_TIMES                                                            = 32, // <Mult> ::= <Mult> '*' <Pow>
        RULE_MULT_DIV                                                              = 33, // <Mult> ::= <Mult> '/' <Pow>
        RULE_MULT                                                                  = 34, // <Mult> ::= <Pow>
        RULE_POW_CARET                                                             = 35, // <Pow> ::= <Pow> '^' <Unary>
        RULE_POW                                                                   = 36, // <Pow> ::= <Unary>
        RULE_UNARY_MINUS                                                           = 37, // <Unary> ::= '-' <Value>
        RULE_UNARY                                                                 = 38, // <Unary> ::= <Value>
        RULE_IFSTMT_IF_THEN_END                                                    = 39, // <IfStmt> ::= IF <Expr> THEN <Stmts> END
        RULE_IFSTMT_IF_THEN_ELSE_END                                               = 40, // <IfStmt> ::= IF <Expr> THEN <Stmts> ELSE <Stmts> END
        RULE_SAYSTMT_SAY_SEMI                                                      = 41, // <SayStmt> ::= SAY <Expr> ';'
        RULE_CANCELSTMT_CANCEL_SEMI                                                = 42, // <CancelStmt> ::= CANCEL ';'
        RULE_CANCELSTMT_CANCEL_ID_SEMI                                             = 43, // <CancelStmt> ::= CANCEL Id ';'
        RULE_ASSIGNSTMT_LET_ID_EQ_SEMI                                             = 44, // <AssignStmt> ::= LET Id '=' <Expr> ';'
        RULE_STMT                                                                  = 45, // <Stmt> ::= <IfStmt>
        RULE_STMT2                                                                 = 46, // <Stmt> ::= <AssignStmt>
        RULE_STMT3                                                                 = 47, // <Stmt> ::= <CancelStmt>
        RULE_STMT4                                                                 = 48, // <Stmt> ::= <SayStmt>
        RULE_VALUE_LPAREN_RPAREN                                                   = 49, // <Value> ::= '(' <Expr> ')'
        RULE_VALUE_ID                                                              = 50, // <Value> ::= Id
        RULE_VALUE                                                                 = 51, // <Value> ::= <Constant>
        RULE_VALUE2                                                                = 52, // <Value> ::= <BoolFunc>
        RULE_VALUE3                                                                = 53, // <Value> ::= <RealFunc>
        RULE_VALUE4                                                                = 54, // <Value> ::= <StringFunc>
        RULE_BOOLFUNC_TEXTIN_LPAREN_COMMA_STRINGLITERAL_RPAREN                     = 55, // <BoolFunc> ::= TEXTIN '(' <Expr> ',' StringLiteral ')'
        RULE_REALFUNC_MINUTES_LPAREN_COMMA_RPAREN                                  = 56, // <RealFunc> ::= MINUTES '(' <Expr> ',' <Expr> ')'
        RULE_REALFUNC_HOURS_LPAREN_COMMA_RPAREN                                    = 57, // <RealFunc> ::= HOURS '(' <Expr> ',' <Expr> ')'
        RULE_STRINGFUNC_LEFT_DOLLAR_LPAREN_COMMA_INTLITERAL_RPAREN                 = 58, // <StringFunc> ::= LEFT '$' '(' <Expr> ',' IntLiteral ')'
        RULE_STRINGFUNC_RIGHT_DOLLAR_LPAREN_COMMA_INTLITERAL_RPAREN                = 59, // <StringFunc> ::= RIGHT '$' '(' <Expr> ',' IntLiteral ')'
        RULE_STRINGFUNC_MID_DOLLAR_LPAREN_COMMA_INTLITERAL_COMMA_INTLITERAL_RPAREN = 60  // <StringFunc> ::= MID '$' '(' <Expr> ',' IntLiteral ',' IntLiteral ')'
    };

        // this class will construct a parser without having to process
        //  the CGT tables with each creation.  It must be initialized
        //  before you can call CreateParser()
    public sealed class ParserFactory
    {
        static Grammar m_grammar;
        static bool _init;
        
        private ParserFactory()
        {
        }
        
        private static BinaryReader GetResourceReader(string resourceName)
        {  
            Assembly assembly = Assembly.GetExecutingAssembly();   
            Stream stream = assembly.GetManifestResourceStream(resourceName);
            return new BinaryReader(stream);
        }
        
        public static void InitializeFactoryFromFile(string FullCGTFilePath)
        {
            if (!_init)
            {
               BinaryReader reader = new BinaryReader(new FileStream(FullCGTFilePath,FileMode.Open));
               m_grammar = new Grammar( reader );
               _init = true;
            }
        }
        
        public static void InitializeFactoryFromResource(string resourceName)
        {
            if (!_init)
            {
                BinaryReader reader = GetResourceReader(resourceName);
                m_grammar = new Grammar( reader );
                _init = true;
            }
        }
        
        public static Parser CreateParser(TextReader reader)
        {
           if (_init)
           {
                return new Parser(reader, m_grammar);
           }
           throw new Exception("You must first Initialize the Factory before creating a parser!");
        }
    }
        
    public abstract class ASTNode
    {
        public abstract bool IsTerminal
        {
            get;
        }
    }
    
    /// <summary>
    /// Derive this class for Terminal AST Nodes
    /// </summary>
    public class TerminalNode : ASTNode
    {
        private Symbol m_symbol;
        private string m_text;
        private int m_lineNumber;
        private int m_linePosition;

        public TerminalNode(Parser theParser)
        {
            m_symbol = theParser.TokenSymbol;
            m_text = theParser.TokenSymbol.ToString();
            m_lineNumber = theParser.LineNumber;
            m_linePosition = theParser.LinePosition;
        }

        public override bool IsTerminal
        {
            get
            {
                return true;
            }
        }
        
        public Symbol Symbol
        {
            get { return m_symbol; }
        }

        public string Text
        {
            get { return m_text; }
        }

        public override string ToString()
        {
            return m_text;
        }

        public int LineNumber 
        {
            get { return m_lineNumber; }
        }

        public int LinePosition
        {
            get { return m_linePosition; }
        }
    }
    
    /// <summary>
    /// Derive this class for NonTerminal AST Nodes
    /// </summary>
    public class NonTerminalNode : ASTNode
    {
        private int m_reductionNumber;
        private Rule m_rule;
        private ArrayList m_array = new ArrayList();

        public NonTerminalNode(Parser theParser)
        {
            m_rule = theParser.ReductionRule;
        }
        
        public override bool IsTerminal
        {
            get
            {
                return false;
            }
        }

        public int ReductionNumber 
        {
            get { return m_reductionNumber; }
            set { m_reductionNumber = value; }
        }

        public int Count 
        {
            get { return m_array.Count; }
        }

        public ASTNode this[int index]
        {
            get { return m_array[index] as ASTNode; }
        }

        public void AppendChildNode(ASTNode node)
        {
            if (node == null)
            {
                return ; 
            }
            m_array.Add(node);
        }

        public Rule Rule
        {
            get { return m_rule; }
        }

    }

    public class MyParser
    {
        MyParserContext m_context;
        ASTNode m_AST;
        string m_errorString;
        Parser m_parser;
        
        public int LineNumber
        {
            get
            {
                return m_parser.LineNumber;
            }
        }

        public int LinePosition
        {
            get
            {
                return m_parser.LinePosition;
            }
        }

        public string ErrorString
        {
            get
            {
                return m_errorString;
            }
        }

        public string ErrorLine
        {
            get
            {
                return m_parser.LineText;
            }
        }

        public ASTNode SyntaxTree
        {
            get
            {
                return m_AST;
            }
        }

        public bool Parse(string source)
        {
            return Parse(new StringReader(source));
        }

        public bool Parse(StringReader sourceReader)
        {
            m_parser = ParserFactory.CreateParser(sourceReader);
            m_parser.TrimReductions = true;
            m_context = new MyParserContext(m_parser);
            
            while (true)
            {
                switch (m_parser.Parse())
                {
                    case ParseMessage.LexicalError:
                        m_errorString = string.Format("Lexical Error. Line {0}. Token {1} was not expected.", m_parser.LineNumber, m_parser.TokenText);
                        return false;

                    case ParseMessage.SyntaxError:
                        StringBuilder text = new StringBuilder();
                        foreach (Symbol tokenSymbol in m_parser.GetExpectedTokens())
                        {
                            text.Append(' ');
                            text.Append(tokenSymbol.ToString());
                        }
                        m_errorString = string.Format("Syntax Error. Line {0}. Expecting: {1}.", m_parser.LineNumber, text.ToString());
                        
                        return false;
                    case ParseMessage.Reduction:
                        m_parser.TokenSyntaxNode = m_context.CreateASTNode();
                        break;

                    case ParseMessage.Accept:
                        m_AST = m_parser.TokenSyntaxNode as ASTNode;
                        m_errorString = null;
                        return true;

                    case ParseMessage.InternalError:
                        m_errorString = "Internal Error. Something is horribly wrong.";
                        return false;

                    case ParseMessage.NotLoadedError:
                        m_errorString = "Grammar Table is not loaded.";
                        return false;

                    case ParseMessage.CommentError:
                        m_errorString = "Comment Error. Unexpected end of input.";
                        
                        return false;

                    case ParseMessage.CommentBlockRead:
                    case ParseMessage.CommentLineRead:
                        // don't do anything 
                        break;
                }
            }
         }

    }

    public class MyParserContext
    {

        // instance fields
        private Parser m_parser;
        
        private TextReader m_inputReader;
        

        
        // constructor
        public MyParserContext(Parser prser)
        {
            m_parser = prser;   
        }
       

        private string GetTokenText()
        {
            // delete any of these that are non-terminals.

            switch (m_parser.TokenSymbol.Index)
            {

                case (int)SymbolConstants.SYMBOL_EOF :
                //(EOF)
                //Token Kind: 3
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ERROR :
                //(Error)
                //Token Kind: 7
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_COMMENT :
                //Comment
                //Token Kind: 2
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_NEWLINE :
                //NewLine
                //Token Kind: 2
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_WHITESPACE :
                //Whitespace
                //Token Kind: 2
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_DIVDIV :
                //'//'
                //Token Kind: 6
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_MINUS :
                //'-'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_DOLLAR :
                //'$'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LPAREN :
                //'('
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_RPAREN :
                //')'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_TIMES :
                //'*'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_COMMA :
                //','
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_DIV :
                //'/'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_SEMI :
                //';'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_CARET :
                //'^'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_PLUS :
                //'+'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LT :
                //'<'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LTEQ :
                //'<='
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LTGT :
                //'<>'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_EQ :
                //'='
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_EQEQ :
                //'=='
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_GT :
                //'>'
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_GTEQ :
                //'>='
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_AND :
                //and
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_CANCEL :
                //CANCEL
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_DATELITERAL :
                //DateLiteral
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_DATETIMELITERAL :
                //DateTimeLiteral
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ELSE :
                //ELSE
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_END :
                //END
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_HOURS :
                //HOURS
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ID :
                //Id
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_IF :
                //IF
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_INTLITERAL :
                //IntLiteral
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LEFT :
                //LEFT
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_LET :
                //LET
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_MID :
                //MID
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_MINUTES :
                //MINUTES
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_OR :
                //or
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_REALLITERAL :
                //RealLiteral
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_RIGHT :
                //RIGHT
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_SAY :
                //SAY
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_STRINGLITERAL :
                //StringLiteral
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_TESTNAME :
                //TestName
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_TEXTIN :
                //TEXTIN
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_THEN :
                //THEN
                //Token Kind: 1
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ADD :
                //<Add>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ARG :
                //<Arg>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ARGS :
                //<Args>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ARGUMENTS :
                //<Arguments>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_ASSIGNSTMT :
                //<AssignStmt>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_BOOLFUNC :
                //<BoolFunc>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_CANCELSTMT :
                //<CancelStmt>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_COMP :
                //<Comp>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_CONSTANT :
                //<Constant>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_EQU :
                //<Equ>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_EXPR :
                //<Expr>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_FUNCTION :
                //<Function>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_IFSTMT :
                //<IfStmt>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_MULT :
                //<Mult>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_POW :
                //<Pow>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_REALFUNC :
                //<RealFunc>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_SAYSTMT :
                //<SayStmt>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_STMT :
                //<Stmt>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_STMTS :
                //<Stmts>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_STRINGFUNC :
                //<StringFunc>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_TESTNAME2 :
                //<TestName>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_UNARY :
                //<Unary>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                case (int)SymbolConstants.SYMBOL_VALUE :
                //<Value>
                //Token Kind: 0
                //todo: uncomment the next line if it's a terminal token ( if Token Kind = 1 )
                // return m_parser.TokenString;
                return null;

                default:
                    throw new SymbolException("You don't want the text of a non-terminal symbol");

            }
            
        }

        public ASTNode CreateASTNode()
        {
            switch (m_parser.ReductionRule.Index)
            {
                case (int)RuleConstants.RULE_FUNCTION :
                //<Function> ::= <TestName> <Arguments> <Stmts>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_TESTNAME_TESTNAME :
                //<TestName> ::= TestName
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARGUMENTS_LPAREN_RPAREN :
                //<Arguments> ::= '(' <Args> ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARGUMENTS_LPAREN_RPAREN2 :
                //<Arguments> ::= '(' ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARG_ID_EQ_REALLITERAL :
                //<Arg> ::= Id '=' RealLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARG_ID_EQ_STRINGLITERAL :
                //<Arg> ::= Id '=' StringLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARG_ID_EQ_DATELITERAL :
                //<Arg> ::= Id '=' DateLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARG_ID_EQ_DATETIMELITERAL :
                //<Arg> ::= Id '=' DateTimeLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARG_ID_EQ_INTLITERAL :
                //<Arg> ::= Id '=' IntLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMTS :
                //<Stmts> ::= <Stmt> <Stmts>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMTS2 :
                //<Stmts> ::= <Stmt>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARGS_COMMA :
                //<Args> ::= <Arg> ',' <Args>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ARGS :
                //<Args> ::= <Arg>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CONSTANT_STRINGLITERAL :
                //<Constant> ::= StringLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CONSTANT_REALLITERAL :
                //<Constant> ::= RealLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CONSTANT_DATELITERAL :
                //<Constant> ::= DateLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CONSTANT_DATETIMELITERAL :
                //<Constant> ::= DateTimeLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CONSTANT_INTLITERAL :
                //<Constant> ::= IntLiteral
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EXPR_OR :
                //<Expr> ::= <Expr> or <Equ>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EXPR_AND :
                //<Expr> ::= <Expr> and <Equ>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EXPR :
                //<Expr> ::= <Equ>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EQU_EQEQ :
                //<Equ> ::= <Equ> '==' <Comp>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EQU_LTGT :
                //<Equ> ::= <Equ> '<>' <Comp>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_EQU :
                //<Equ> ::= <Comp>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_COMP_GT :
                //<Comp> ::= <Comp> '>' <Add>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_COMP_LT :
                //<Comp> ::= <Comp> '<' <Add>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_COMP_GTEQ :
                //<Comp> ::= <Comp> '>=' <Add>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_COMP_LTEQ :
                //<Comp> ::= <Comp> '<=' <Add>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_COMP :
                //<Comp> ::= <Add>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ADD_PLUS :
                //<Add> ::= <Add> '+' <Mult>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ADD_MINUS :
                //<Add> ::= <Add> '-' <Mult>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ADD :
                //<Add> ::= <Mult>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_MULT_TIMES :
                //<Mult> ::= <Mult> '*' <Pow>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_MULT_DIV :
                //<Mult> ::= <Mult> '/' <Pow>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_MULT :
                //<Mult> ::= <Pow>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_POW_CARET :
                //<Pow> ::= <Pow> '^' <Unary>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_POW :
                //<Pow> ::= <Unary>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_UNARY_MINUS :
                //<Unary> ::= '-' <Value>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_UNARY :
                //<Unary> ::= <Value>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_IFSTMT_IF_THEN_END :
                //<IfStmt> ::= IF <Expr> THEN <Stmts> END
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_IFSTMT_IF_THEN_ELSE_END :
                //<IfStmt> ::= IF <Expr> THEN <Stmts> ELSE <Stmts> END
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_SAYSTMT_SAY_SEMI :
                //<SayStmt> ::= SAY <Expr> ';'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CANCELSTMT_CANCEL_SEMI :
                //<CancelStmt> ::= CANCEL ';'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_CANCELSTMT_CANCEL_ID_SEMI :
                //<CancelStmt> ::= CANCEL Id ';'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_ASSIGNSTMT_LET_ID_EQ_SEMI :
                //<AssignStmt> ::= LET Id '=' <Expr> ';'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMT :
                //<Stmt> ::= <IfStmt>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMT2 :
                //<Stmt> ::= <AssignStmt>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMT3 :
                //<Stmt> ::= <CancelStmt>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STMT4 :
                //<Stmt> ::= <SayStmt>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE_LPAREN_RPAREN :
                //<Value> ::= '(' <Expr> ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE_ID :
                //<Value> ::= Id
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE :
                //<Value> ::= <Constant>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE2 :
                //<Value> ::= <BoolFunc>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE3 :
                //<Value> ::= <RealFunc>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_VALUE4 :
                //<Value> ::= <StringFunc>
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_BOOLFUNC_TEXTIN_LPAREN_COMMA_STRINGLITERAL_RPAREN :
                //<BoolFunc> ::= TEXTIN '(' <Expr> ',' StringLiteral ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_REALFUNC_MINUTES_LPAREN_COMMA_RPAREN :
                //<RealFunc> ::= MINUTES '(' <Expr> ',' <Expr> ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_REALFUNC_HOURS_LPAREN_COMMA_RPAREN :
                //<RealFunc> ::= HOURS '(' <Expr> ',' <Expr> ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STRINGFUNC_LEFT_DOLLAR_LPAREN_COMMA_INTLITERAL_RPAREN :
                //<StringFunc> ::= LEFT '$' '(' <Expr> ',' IntLiteral ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STRINGFUNC_RIGHT_DOLLAR_LPAREN_COMMA_INTLITERAL_RPAREN :
                //<StringFunc> ::= RIGHT '$' '(' <Expr> ',' IntLiteral ')'
                //todo: Perhaps create an object in the AST.
                return null;

                case (int)RuleConstants.RULE_STRINGFUNC_MID_DOLLAR_LPAREN_COMMA_INTLITERAL_COMMA_INTLITERAL_RPAREN :
                //<StringFunc> ::= MID '$' '(' <Expr> ',' IntLiteral ',' IntLiteral ')'
                //todo: Perhaps create an object in the AST.
                return null;

                default:
                    throw new RuleException("Unknown rule: Does your CGT Match your Code Revision?");
            }
            
        }

    }
    
}

﻿using System;

namespace LIS_BASIC
{
	public class FunctionLog : SimpleExpression
	{
		private readonly SimpleExpression _expr;

		public FunctionLog(Context context, SimpleExpression expr)
			: base (context)
		{
			_expr = expr;
		}



		public override object Value
		{
			get
			{
				double v = Convert.ToDouble(_expr.Value.ToString());
				double lv = Math.Log10(v);

				if (Double.IsNaN(lv))
					Context.ErrorInCalculation = true;

				return lv;
			}
		}



	}
}

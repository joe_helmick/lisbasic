﻿namespace LIS_BASIC
{
	public class SimpleStatementList : SimpleStatement
	{
		private readonly SimpleStatement _mCurrentStatement;
		private readonly SimpleStatement _mNextStatement;

		public SimpleStatementList(Context context,
			SimpleStatement currentStatement, SimpleStatement nextStatement)
			: base(context)
		{
			_mCurrentStatement = currentStatement;
			_mNextStatement = nextStatement;
		}

		public override void Execute()
		{

			_mCurrentStatement.Execute();
			if (_mNextStatement != null && Context.ExecutionHalted == false)
			{
				_mNextStatement.Execute();
			}
		}
	}
}

﻿namespace LIS_BASIC
{
	public class SimpleInt : SimpleExpression
	{
		private readonly int _mValue;

		public SimpleInt(Context context, int value)
			: base(context)
		{
			_mValue = value;
		}

		public override object Value
		{
			get { return _mValue; }
		}
	}
}

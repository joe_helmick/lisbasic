﻿using System;



namespace LIS_BASIC
{
	public class SimpleId : SimpleExpression
	{
		private readonly string _name;

		public SimpleId(Context context, string name)
			: base(context)
		{
			// Convert all variable names to uppercase internally.
			_name = name.ToUpper();
		}


		public string Name
		{
			get
			{
				return _name.ToUpper();
			}
		}

		public override object Value
		{
			get
			{
				if (Context.Variables[_name] == null && _name != Context.TestName)
					throw new SymbolException("The symbol " + _name + " has a null value.  Did you misspell a variable or test name?");

				try { return Context.Variables[_name]; }
				catch (Exception)
				{
					throw new SymbolException("The symbol " + _name + " is not known.  Did you misspell a variable or test name?");
				}
				
			}
		}
	}
}

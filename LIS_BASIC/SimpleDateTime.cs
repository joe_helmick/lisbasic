﻿using System;



namespace LIS_BASIC
{
	public class SimpleDateTime : SimpleExpression
	{
		private readonly DateTime _mValue;

		public SimpleDateTime(Context context, DateTime value)
			: base(context)
		{
			_mValue = value;
		}

		public override object Value
		{
			get { return _mValue; }
		}
	}
}

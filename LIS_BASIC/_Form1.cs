﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using GoldParser;
using System.Xml;
using System.Xml.Linq;


namespace LIS_BASIC
{


	public partial class Form1 : Form
	{
		#region CONSTRUCTOR  AND FORM LOAD HANDLER

		public Form1()
		{
			InitializeComponent();
		}


		private void _Form1_Load(object sender, EventArgs e)
		{

			// Time all the startup events, hopefully one-time startup costs.
			Stopwatch sw = new Stopwatch();
			sw.Start();

			// Load the GOLD Parser CGT file
			string cgtfilename = Application.StartupPath + @"..\..\..\..\LIS_BASIC.cgt";
			using (Stream cgtStream = File.OpenRead(cgtfilename))
			{
				BinaryReader cgtReader = new BinaryReader(cgtStream);
				_grammar = new Grammar(cgtReader);
			}

			// Load the last source file if possible.
			if (Settings1.Default.lastFileName != null)
			{
				sourcefilename = Settings1.Default.lastFileName;
				LoadSourceFile(sourcefilename);
			}

			// Turn off error indicator until it's needed.
			stError.Visible = false;

			// Load the stock comments (from whatever sort of repository).
			BasicEnvironment.LoadStockComments();
			sw.Stop();
			Debug.WriteLine("load: {0} ms", sw.Elapsed.Ticks / 10000F);
		}



		#endregion

		#region PRIVATE MEMBERS

		private Parser _parser;
		private Grammar _grammar;
		private Context _context;
		private SimpleStatement _program;
		private string _errorMessage;
		private string _lastSuccessfulToken = "";
		private string sourcefilename;

		#endregion


		private void ResetUIElements()
		{
			etResult.Text = "";
			etErrorMessage.Text = "";
			etSayComment.Text = "";
			stError.Visible = false;
		}

		private void LoadSourceFile(string filename)
		{
			if (filename == "") return;
			StreamReader sr = new StreamReader(filename);
			string source = sr.ReadToEnd();
			sr.Close();
			etCode2.Text = source;
			ResetUIElements();
			Settings1.Default.lastFileName = filename;
			Settings1.Default.Save();
			Text = "LIS Calculated Results Tester [" + filename + "]";
		}


		private void LoadContext()
		{
			// Add stuff to context.
			_context.TestsInOrder.Add("@WBCIR");
			_context.TestsInOrder.Add("@NRBC");
			_context.TestsInOrder.Add("@WBCI");

		}

		private void pbRun_Click(object sender, EventArgs e)
		{
			ResetUIElements();

			// Create a timer.
			Stopwatch sw = new Stopwatch();

			// Time the parsing step.
			sw.Start();
			bool parseSuccess = DoParse();
			sw.Stop();
			Debug.WriteLine("parse: {0} ms", sw.Elapsed.Ticks/10000F);

			// Reset the stopwatch.
			sw.Reset();

			if (parseSuccess)
			{
				// Reset error in calculation flag.
				_context.ErrorInCalculation = false;


				// Load the execution context in preparation to execute;
				LoadContext();

				// Restart the stopwatch to time program execution.
				sw.Restart();

				// Execute the program.
				if (_program != null)
					try { _program.Execute(); }
					catch (SymbolException ex)
					{
						etErrorMessage.Text = ex.Message;
						return;
					}
					catch (System.Collections.Generic.KeyNotFoundException ex)
					{
						etErrorMessage.Text = ex.Message;
						return;
					}
					catch (ArgumentOutOfRangeException ex)
					{
						etErrorMessage.Text = ex.Message;
						return;
					}
					catch (Exception ex)
					{
						etErrorMessage.Text = ex.Message;
						return;
					}

				sw.Stop();
				Debug.WriteLine("execute: {0} ms", sw.Elapsed.Ticks / 10000F);
				sw.Reset();

				if (_context.ErrorInCalculation)
					stError.Visible = true;

				if (_context.SayValue != null)
					etResult.Text = _context.SayValue.ToString();

				// Show canceled tests.
				if (_context.Cancels.Count > 0)
				{
					StringBuilder sbcanceled = new StringBuilder();
					foreach (string test in _context.Cancels)
						sbcanceled.Append(test.ToUpper() + " ");
					etErrorMessage.Text = "CANCELED: " + sbcanceled;
				}

				// Show comments that would appear bottom of report.
				if (_context.Comments.Count > 0)
				{
					StringBuilder sbComments = new StringBuilder();
					foreach (string comment in _context.Comments)
						sbComments.Append(comment + " ");
					etSayComment.Text = sbComments.ToString();
				}

			}
			else
			{
				Debug.WriteLine("SYNTAX OR LEXICAL ERROR ERROR");
				etErrorMessage.Text = _errorMessage;
			}
		}


		private bool DoParse()
		{
			StringReader reader = new StringReader(etCode2.Text);
			_parser = new Parser(reader, _grammar) { TrimReductions = true };

			_context = new Context(_parser);
			while (true)
			{
				switch (_parser.Parse())
				{
					case ParseMessage.LexicalError :
						_errorMessage = "LEXICAL ERROR on line " + _parser.LineNumber + ", position " + _parser.LinePosition + ". Cannot recognize token: " + _parser.TokenText;
						etCode2.SelectionStart = _parser.CharPosition;
						etCode2.Focus();
						return false;

					case ParseMessage.SyntaxError :

						StringBuilder sbText = new StringBuilder();
						foreach (Symbol tokenSymbol in _parser.GetExpectedTokens())
							sbText.Append(' ' + tokenSymbol.ToString());
						_errorMessage = _parser.LineText + Environment.NewLine;
						_errorMessage += String.Format("Syntax error on line {0}, position {1}.  The last word read was {2}.  Expecting one of the following: {3}", _parser.LineNumber, _parser.LinePosition, _lastSuccessfulToken, sbText);
						etCode2.SelectionStart = _parser.CharPosition;
						etCode2.Focus();
						return false;

					case ParseMessage.Reduction :

						//== Create a new customized object and replace the
						//== CurrentReduction with it. This saves memory and allows
						//== easier interpretation
						_parser.TokenSyntaxNode = _context.GetSyntaxNode();
						break;

					case ParseMessage.Accept :

						//=== Success!
						_program = (SimpleStatement)_parser.TokenSyntaxNode;
						return true;

					case ParseMessage.TokenRead :

						//=== Make sure that we store token string for needed tokens.
						try
						{
							_parser.TokenSyntaxNode = _context.GetTokenText();
							_lastSuccessfulToken = _context.GetTokenText();
						}
						catch (Exception)
						{
							_errorMessage = "BAD TOKEN";
						}
						break;

					case ParseMessage.InternalError :
						Debug.WriteLine("INTERNAL ERROR! Something is horribly wrong");
						return false;

					case ParseMessage.NotLoadedError :

						//=== Due to the if-statement above, this case statement should never be true
						Debug.WriteLine("NOT LOADED ERROR! Compiled Grammar Table not loaded");
						return false;

					case ParseMessage.CommentError :
						Debug.WriteLine("COMMENT ERROR! Unexpected end of file");
						return false;

					case ParseMessage.CommentBlockRead :

						//=== Do nothing
						break;

					case ParseMessage.CommentLineRead :

						//=== Do nothing
						break;
				}
			}
		}


		#region MENU ITEM HANDLERS

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Filter = "BASIC files (*.bas;*.txt)|*.bas;*.txt";
			ofd.FilterIndex = 0;
			DialogResult dr = ofd.ShowDialog();
			if (dr == DialogResult.OK)
			{
				sourcefilename = ofd.FileName;
				Settings1.Default.lastFileName = sourcefilename;
				Settings1.Default.Save();
				LoadSourceFile(sourcefilename);
			}
		}


		private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			LoadSourceFile(sourcefilename);
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void mnuFileSave_Click(object sender, EventArgs e)
		{
			StreamWriter sw = new StreamWriter(sourcefilename);
			sw.Write(etCode2.Text);
			sw.Close();
		}

		private void mnuFileSaveAs_Click(object sender, EventArgs e)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.Filter = "BASIC files (*.bas;*.txt)|*.bas;*.txt";
			DialogResult dr = sfd.ShowDialog();
			if (dr == System.Windows.Forms.DialogResult.OK)
			{
				sourcefilename = sfd.FileName;
				StreamWriter sw = new StreamWriter(sourcefilename);
				sw.Write(etCode2.Text);
				sw.Close();
				LoadSourceFile(sourcefilename);
			}
		}

		private void mnuFileNew_Click(object sender, EventArgs e)
		{
			if (sourcefilename != "" && etCode2.Text != "")
			{
				DialogResult dr = MessageBox.Show("Discard the current file and any changes and start with a blank file?", "Are You Sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
				if (dr == System.Windows.Forms.DialogResult.No) return;

				sourcefilename = "";
				etCode2.Text = "";
				etErrorMessage.Text = "";
				etResult.Text = "";
				etSayComment.Text = "";
				stError.Visible = false;
				Text = "LIS Calculated Results Tester [new file]";
			}
		}



		#endregion

		private void etCode2_Load(object sender, EventArgs e)
		{
		}

	}


}
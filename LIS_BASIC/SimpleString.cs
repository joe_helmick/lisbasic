﻿namespace LIS_BASIC
{
	public class SimpleString : SimpleExpression
	{
		private readonly string _mValue;

		public SimpleString(Context context, string text)
			: base(context)
		{
			_mValue = text;
		}

		public override object Value
		{
			get { return _mValue; }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIS_BASIC
{
	public class SayCommentStatement : SimpleStatement
	{
		private readonly string _id;

		public SayCommentStatement(Context context, string id)
			: base(context)
		{
			_id = id;
		}

		public override void Execute()
		{
			try
			{
				var item = BasicEnvironment.CommentsRepository[_id];
				Context.Comments.Add(item);
			}
			catch (KeyNotFoundException)
			{
				throw new KeyNotFoundException("Unknown comment code: " + _id);
			}
		}
	}
}

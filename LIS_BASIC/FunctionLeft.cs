﻿using System;



namespace LIS_BASIC
{
	public class FunctionLeft : SimpleExpression
	{
		private readonly SimpleExpression _text;
		private readonly int _chars;

		public FunctionLeft(Context context, SimpleExpression text, int chars)
			: base(context)
		{
			_text = text;
			_chars = chars;
		}

		public override object Value
		{
			get
			{
				try
				{
					if (_chars >= _text.ToString().Length)
						return _text.Value.ToString();
					return _text.Value.ToString().Substring(0, _chars);
				}
				catch
				{
					throw new ArgumentOutOfRangeException("length", "ERROR: The number of characters to show must be positive.");
				}
			}
		}
	}
}

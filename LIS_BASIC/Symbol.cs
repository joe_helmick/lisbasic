﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIS_BASIC
{
	public enum EDataTypes
	{
		string_type = 0,
		real_type = 1,
		date_type = 2,
		bool_type = 3,
		int_type = 4,
	}







	public class Symbol
	{
		public string Name { get; set; }
		public EDataTypes Type { get; set; }

		public string StringData { get; set; }
		public Double DoubleData { get; set; }
		public DateTime DateTimeData { get; set; }
		public bool BoolData { get; set; }
		public int IntData { get; set; }


		public Symbol(double value)
		{
			Type = EDataTypes.real_type;
			DoubleData = value;
			Name = Context.MakeSymbolName();
		}


		public Symbol(string name, double value)
			: this(value)
		{
			Name = name;
		}


		public Symbol(string value)
		{
			Type = EDataTypes.string_type;
			StringData = value;
			Name = Context.MakeSymbolName();
		}


		public Symbol(string name, string value)
			: this(value)
		{
			Name = name;
		}


		public Symbol(DateTime value)
		{
			Type = EDataTypes.date_type;
			DateTimeData = value;
			Name = Context.MakeSymbolName();
		}


		public Symbol(string name, DateTime value)
			: this(value)
		{
			Name = name;
		}


		public Symbol(bool value)
		{
			Type = EDataTypes.bool_type;
			BoolData = value;
			Name = Context.MakeSymbolName();
		}


		public Symbol(string name, bool value)
			: this(value)
		{
			Name = name;
		}


		public Symbol(int value)
		{
			Type = EDataTypes.int_type;
			IntData = value;
			Name = Context.MakeSymbolName();
		}


		public Symbol(string name, int value)
			: this(value)
		{
			Name = name;
		}




		public override string ToString()
		{
			switch (Type)
			{
				case EDataTypes.real_type:
					return Name + ":" + Type + ":" + DoubleData;
				case EDataTypes.bool_type:
					return Name + ":" + Type + ":" + BoolData;
				case EDataTypes.date_type:
					return Name + ":" + Type + ":" + DateTimeData.ToString("s");
				case EDataTypes.int_type:
					return Name + ":" + Type + ":" + IntData;
				case EDataTypes.string_type:
					return Name + ":" + Type + ":" + StringData;
			}
			return "";
		}



	}

}

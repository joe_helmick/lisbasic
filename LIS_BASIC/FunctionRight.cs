﻿using System;



namespace LIS_BASIC
{
	public class FunctionRight : SimpleExpression
	{
		private readonly SimpleExpression _text;
		private readonly int _chars;

		public FunctionRight(Context context, SimpleExpression text, int chars)
			: base(context)
		{
			_text = text;
			_chars = chars;
		}

		public override object Value
		{
			get
			{
				try
				{
					string target = _text.Value.ToString();
					if (_chars >= target.Length)
						return target;
					return target.Substring(target.Length - _chars, _chars);
				}
				catch
				{
					throw new ArgumentOutOfRangeException("length", "ERROR: The number of characters to show must be positive.");
				}
			}
		}
	}
}

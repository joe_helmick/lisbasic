﻿using System;



namespace LIS_BASIC
{
	class FunctionMinutesDiff : SimpleExpression
	{
		private readonly SimpleExpression _date1;
		private readonly SimpleExpression _date2;

		public FunctionMinutesDiff(Context context, SimpleExpression date1, SimpleExpression date2)
			: base(context)
		{
			_date1 = date1;
			_date2 = date2;
		}

		public override object Value
		{
			get
			{
				DateTime d1 = Convert.ToDateTime(_date1.Value.ToString());
				DateTime d2 = Convert.ToDateTime(_date2.Value.ToString());
				TimeSpan ts = (d1 - d2);
				return ts.TotalMinutes;
			}
		}
	}
}

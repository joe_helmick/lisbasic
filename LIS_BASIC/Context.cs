﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using GoldParser;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;


namespace LIS_BASIC
{


	public class Context
	{
#region  CONSTRUCTOR AND PUBLIC MEMBERS


		public Context(Parser parser)
		{
			_parser = parser;
			Cancels = new List <string>();
			TestsInOrder = new List <string>();
			Comments = new List<string>();
			Decimals = -1;
			TestName = "";
		}

		public VariableList Variables
		{
			get
			{
				return _variables;
			}
		}
		public List<string> TestsInOrder;
		public List<string> Cancels;
		public List<string> Comments = new List<string>();
		public object SayValue;
		public string TestName;
		public bool ErrorInCalculation;
		public bool ExecutionHalted;
		public int Decimals;

#endregion

#region PRIVATE MEMBERS

		private readonly VariableList _variables = new VariableList();
		private readonly Parser _parser;
		private SimpleId _thisTest;

#endregion


		public string GetTokenText()
		{
			// Include only useful terminals in this list.
			switch (_parser.TokenSymbol.Index)
			{
				case (int)SymbolConstants.symbol_eof :
					return null;
				case (int)SymbolConstants.symbol_minus : //'-'
				case (int)SymbolConstants.symbol_ampersand: // '&'
				case (int)SymbolConstants.symbol_lparen : //'('
				case (int)SymbolConstants.symbol_rparen : //')'
				case (int)SymbolConstants.symbol_times : //'*'
				case (int)SymbolConstants.symbol_comma : //','
				case (int)SymbolConstants.symbol_div : //'/'
				case (int)SymbolConstants.symbol_caret : //'^'
				case (int)SymbolConstants.symbol_plus : //'+'
				case (int)SymbolConstants.symbol_lt : //'<'
				case (int)SymbolConstants.symbol_lteq : //'<='
				case (int)SymbolConstants.symbol_ltgt : //'<>'
				case (int)SymbolConstants.symbol_eq : //'='
				case (int)SymbolConstants.symbol_eqeq : //'=='
				case (int)SymbolConstants.symbol_gt : //'>'
				case (int)SymbolConstants.symbol_gteq : //'>='
				case (int)SymbolConstants.symbol_and : //and
				case (int)SymbolConstants.symbol_cancel : //CANCEL
				case (int)SymbolConstants.symbol_dateliteral : //DateLiteral
				case (int)SymbolConstants.symbol_datetimeliteral : //DateTimeLiteral
				case (int)SymbolConstants.symbol_daysdiff : // DAYSDIFF
				case (int)SymbolConstants.symbol_else : //ELSE
				case (int)SymbolConstants.symbol_end : //END
				case (int)SymbolConstants.symbol_hoursdiff :  // HOURSDIFF
				case (int)SymbolConstants.symbol_id : //Id
				case (int)SymbolConstants.symbol_if : //IF
				case (int)SymbolConstants.symbol_intliteral : //IntLiteral
				case (int)SymbolConstants.symbol_left : //LEFT
				case (int)SymbolConstants.symbol_let : //LET
				case (int)SymbolConstants.symbol_mid : //MID
				case (int)SymbolConstants.symbol_minutesdiff : // MINUTESDIFF
				case (int)SymbolConstants.symbol_not : // NOT
				case (int)SymbolConstants.symbol_or : //or
				case (int)SymbolConstants.symbol_orderhas : // ORDERHAS
				case (int)SymbolConstants.symbol_realliteral : //RealLiteral
				case (int)SymbolConstants.symbol_right : //RIGHT
				case (int)SymbolConstants.symbol_say : //SAY
				case (int)SymbolConstants.symbol_stringliteral : //StringLiteral
				case (int)SymbolConstants.symbol_textin : //TEXTIN
				case (int)SymbolConstants.symbol_then : //THEN
				case (int)SymbolConstants.symbol_yearsdiff: // YEARSDIFF
				case (int)SymbolConstants.symbol_number:
				case (int)SymbolConstants.symbol_log:
				case (int)SymbolConstants.symbol_saycomment:
				case (int)SymbolConstants.symbol_halt:
				case (int)SymbolConstants.symbol_decimals:
					{
						return _parser.TokenText;
					}
				default :
					Debug.WriteLine("UNHANDLED SYMBOL: {0} {1}", _parser.TokenString, _parser.TokenSymbol.Index);
					throw new SymbolException("You don't want the text of a non-terminal symbol");
			}
		}


		public SimpleSyntaxNode GetSyntaxNode()
		{
			switch (_parser.ReductionRule.Index)
			{
					#region   MAIN FUNCTION 


				case (int)RuleConstants.rule_function : //<Function> ::= <TestName> <Arguments> <Stmts>
					return new SimpleStatementList(this, Statement(2), null);


					#endregion


					#region  TESTNAME AND ARGUMENTS


				case (int)RuleConstants.rule_testname_id : //<TestName> ::= Id
					_thisTest = new SimpleId(this, Token(0));
					Variables.Add(_thisTest.Name, null);
					TestName = Token(0).ToUpper();
					return null;

				case (int)RuleConstants.rule_arg_id_eq_realliteral : //<Arg> ::= Id '=' RealLiteral
					Variables.Add(Token(0), Convert.ToDouble(Token(2)));
					return new SimpleId(this, Token(0));

				case (int)RuleConstants.rule_arg_id_eq_stringliteral : //<Arg> ::= Id '=' StringLiteral
					Variables.Add(Token(0), Token(2).Replace("\"", ""));
					return new SimpleId(this, Token(0));

				case (int)RuleConstants.rule_arg_id_eq_dateliteral : //<Arg> ::= Id '=' DateLiteral
				{
					string name = Token(0);
					DateTime value = Convert.ToDateTime(Token(2));
					Variables.Add(name, value);
					return new SimpleId(this, Token(0));
				}

				case (int)RuleConstants.rule_arg_id_eq_datetimeliteral :
				{
					string name = Token(0);
					DateTime value = TokenToDateTime(Token(2));
					Variables.Add(name, value);
					return new SimpleId(this, Token(0));
				}

				case (int)RuleConstants.rule_arg_id_eq_intliteral :
					Variables.Add(Token(0), Convert.ToInt32(Token(2)));
					return new SimpleId(this, Token(0));


					#endregion


					#region  STATEMENT = STATEMEMT STATEMENTLIST


				case (int)RuleConstants.rule_stmts : //<Stmts> ::= <Stmt> <Stmts>
					return new SimpleStatementList(this, Statement(0), Statement(1));

				case (int)RuleConstants.rule_stmts2 : //<Stmts> ::= <Stmt>
					return new SimpleStatementList(this, Statement(0), null);


					#endregion


					#region  CONST = LITERAL


				case (int)RuleConstants.rule_constant_stringliteral :
//					Debug.WriteLine("Const = " + Token(0).Replace("\"", ""));
					return new SimpleString(this, Token(0).Replace("\"", ""));

				case (int)RuleConstants.rule_constant_realliteral : //<Constant> ::= RealLiteral
//					Debug.WriteLine("RealLiteral = " + Token(0));
					return new SimpleReal(this, Convert.ToDouble(Token(0)));

				case (int)RuleConstants.rule_constant_dateliteral :
//					Debug.WriteLine("DateLiteral = " + Token(0));
					return new SimpleDateTime(this, Convert.ToDateTime(Token(0)));

				case (int)RuleConstants.rule_constant_datetimeliteral :
//					Debug.WriteLine("DateTimeLiteral = " + Token(0));
					return new SimpleDateTime(this, TokenToDateTime(Token(0)));

				case (int)RuleConstants.rule_constant_intliteral :
//					Debug.WriteLine("IntLiteral = " + Token(0));
//					Debug.WriteLine(Token(0));
					return new SimpleInt(this, Convert.ToInt32(Token(0)));


					#endregion


					#region  BINARY EXPRESSIONS


				case (int)RuleConstants.rule_expr_or : //<Expr> ::= <Expr> or <Equ>
				case (int)RuleConstants.rule_expr_and : //<Expr> ::= <Expr> and <Equ>
				case (int)RuleConstants.rule_equ_eqeq : //<Equ> ::= <Equ> '==' <Comp>
				case (int)RuleConstants.rule_equ_ltgt : //<Equ> ::= <Equ> '<>' <Comp>
				case (int)RuleConstants.rule_comp_gt : //<Comp> ::= <Comp> '>' <Add>
				case (int)RuleConstants.rule_comp_lt : //<Comp> ::= <Comp> '<' <Add>
				case (int)RuleConstants.rule_comp_gteq : //<Comp> ::= <Comp> '>=' <Add>
				case (int)RuleConstants.rule_comp_lteq : //<Comp> ::= <Comp> '<=' <Add>
				case (int)RuleConstants.rule_add_plus : //<Add> ::= <Add> '+' <Mult>
				case (int)RuleConstants.rule_concat: //  <Add> ::= <Add> '&' <Mult> 
				case (int)RuleConstants.rule_add_minus : //<Add> ::= <Add> '-' <Mult>
				case (int)RuleConstants.rule_mult_times : //<Mult> ::= <Mult> '*' <Pow>
				case (int)RuleConstants.rule_mult_div : //<Mult> ::= <Mult> '/' <Pow>
				case (int)RuleConstants.rule_pow_caret : //<Pow> ::= <Pow> '^' <Unary>
					return new SimpleBinaryExpression(this, Expression(0), Token(1), Expression(2));

					#endregion


					#region   UNARY EXPRESSIONS

				
				case (int)RuleConstants.rule_unary_not : //<Unary> ::= NOT <Value>
					return new SimpleUnaryExpression(this, Expression(1), Token(0));

				case (int)RuleConstants.rule_unary_minus : //<Unary> ::= '-' <Value>
					return new SimpleUnaryExpression(this, Expression(1), Token(0));


					#endregion


					#region   STATEMENTS


				case (int)RuleConstants.rule_ifstmt_if_then_end : //<IfStmt> ::= IF <Expr> THEN <Stmts> END
					return new SimpleIfStatement(this, Expression(1), Statement(3), null);

				case (int)RuleConstants.rule_ifstmt_if_then_else_end : //<IfStmt> ::= IF <Expr> THEN <Stmts> ELSE <Stmts> END
					return new SimpleIfStatement(this, Expression(1), Statement(3), Statement(5));

				case (int)RuleConstants.rule_saystmt_say_expr : //<SayStmt> ::= SAY <Expr> 
					return new SayStatement(this, null, Expression(1), null);

				case (int)RuleConstants.rule_saystmt_say_sl_expr: //<SayStmt> ::= SAY StringLiteral ',' <Expr> 
						return new SayStatement(this, Token(1), Expression(3), null);

				case (int)RuleConstants.rule_saystmt_say_sl_expr_sl: //<SayStmt> ::= SAY StringLiteral ',' <Expr> ',' StringLiteral
						return new SayStatement(this, Token(1), Expression(3), Token(5));

				case (int)RuleConstants.rule_cancelstmt : //<CancelStmt> ::= CANCEL 
					return new CancelStatement(this, _thisTest);

				case (int)RuleConstants.rule_cancelstmt_id : //<CancelStmt> ::= CANCEL Id 
					return new CancelStatement(this,new SimpleId(this, Token(1)));

				case (int)RuleConstants.rule_assignstmt : //<AssignStmt> ::= LET Id '=' <Expr>
					return new SimpleAssignmentStatement(this, Token(1), Expression(3));

				case (int)RuleConstants.rule_commentstmt : // <CommentStmt> ::= SAYCOMMENT Id 
					return new SayCommentStatement(this, Token(1));

				case (int)RuleConstants.rule_haltstmt: // <HaltStmt> ::= HALT 
					return new HaltStatement(this);

				case (int)RuleConstants.rule_decimals: // <DecimalsStmt> ::= DECIMALS Intliteral
					return new DecimalsStatement(this, Convert.ToInt32(Token(1)));
					#endregion

				case (int)RuleConstants.rule_value_lparen_rparen :
					return Expression(1);

				case (int)RuleConstants.rule_value_id : //<Value> ::= Id
					Variables.Add(Token(0), null);
					return new SimpleId(this, Token(0));


					#region   FUNCTIONS


				case (int)RuleConstants.rule_boolfunc_textin : //<BoolFunc> ::= TEXTIN '(' <Expr> ',' StringLiteral ')'
					return new FunctionTextIn(this, Expression(2), Token(4).Replace("\"", ""));

				case (int)RuleConstants.rule_boolfunc_orderhas : // <BoolFunc> ::= ORDERHAS '(' Id ')'
					return new FunctionOrderHas(this, Token(2));

				case (int)RuleConstants.rule_realfunc_minutesdiff :
					return new FunctionMinutesDiff(this, Expression(2), Expression(4));

				case (int)RuleConstants.rule_realfunc_hoursdiff :
					return new FunctionHoursDiff(this, Expression(2), Expression(4));

				case (int)RuleConstants.rule_realfunc_daysdiff : // DAYSDIFF ( EXPR1 , EXPR2)
					return new FunctionDaysDiff(this, Expression(2), Expression(4));

				case (int)RuleConstants.rule_realfunc_yearsdiff: // YEARSDIFF ( EXPR1 , EXPR2)
					return new FunctionYearsDiff(this, Expression(2), Expression(4));

				case (int)RuleConstants.rule_stringfunc_left : // LEFT  ( <expr> , intliteral )
					return new FunctionLeft(this, Expression(2), Convert.ToInt32(Token(4)));

				case (int)RuleConstants.rule_stringfunc_right : // RIGHT ( <expr> , intliteral )
					return new FunctionRight(this, Expression(2), Convert.ToInt32(Token(4)));

				case (int)RuleConstants.rule_stringfunc_mid : // MID ( <expr> , intliteralstart, intliteralcount )
					return new FunctionMid(this, Expression(2), Convert.ToInt32(Token(4)), Convert.ToInt32(Token(6)));

				case (int) RuleConstants.rule_realfunc_number:  // NUMBER ( <expr> )
					return new FunctionNumber(this, Expression(2));

				case (int) RuleConstants.rule_realfunc_log: // LOG ( <expr ) 
					return new FunctionLog(this, Expression(2));

					#endregion


					#region  NON-EVALUATED RULES

	
				case (int)RuleConstants.rule_args_comma : //<Args> ::= <Arg> ',' <Args>
				return null;
				case (int)RuleConstants.rule_arguments_lparen_rparen: //<Arguments> ::= '(' <Args> ')'
				return null;
				case (int)RuleConstants.rule_arguments_lparen_rparen2: //<Arguments> ::= '(' ')'
				return null;
				case (int)RuleConstants.rule_args: //<Args> ::= <Arg>
				return null;
				case (int)RuleConstants.rule_expr: //<Expr> ::= <Equ>
				return null;
				case (int)RuleConstants.rule_comp: //<Comp> ::= <Add>
				return null;
				case (int)RuleConstants.rule_equ: //<Equ> ::= <Comp>
				return null;
				case (int)RuleConstants.rule_add: //<Add> ::= <Mult>
				return null;
				case (int)RuleConstants.rule_unary: //<Unary> ::= <Value>
				return null;
				case (int)RuleConstants.rule_pow: //<Pow> ::= <Unary>
				return null;
				case (int)RuleConstants.rule_mult: //<Mult> ::= <Pow>
				return null;
				case (int)RuleConstants.rule_stmt: //<Stmt> ::= <IfStmt>
				return null;
				case (int)RuleConstants.rule_stmt2: //<Stmt> ::= <AssignStmt>
				return null;
				case (int)RuleConstants.rule_stmt3: //<Stmt> ::= <CancelStmt>
				return null;
				case (int)RuleConstants.rule_stmt4: //<Stmt> ::= <SayStmt>
				return null;
				case (int)RuleConstants.rule_value: //<Value> ::= <Constant>
				return null;
				case (int)RuleConstants.rule_value2: //<Value> ::= <BoolFunc>
				return null;
				case (int)RuleConstants.rule_value3: //<Value> ::= <RealFunc>
				return null;
				case (int)RuleConstants.rule_value4: //<Value> ::= <StringFunc>
					return null;


					#endregion


				default :
					Debug.WriteLine("UNKNOWN REDUCTION: " + _parser.ReductionRule.Definition);
					return null;
			}
		}


		#region  HELPER METHODS


		private SimpleExpression Expression(int index)
		{
			return (SimpleExpression)_parser.GetReductionSyntaxNode(index);
		}


		private SimpleStatement Statement(int index)
		{
			return (SimpleStatement)_parser.GetReductionSyntaxNode(index);
		}


		private string Token(int index)
		{
			return (string)_parser.GetReductionSyntaxNode(index);
		}


		private static DateTime TokenToDateTime(string token)
		{
			string yearpart = token.Substring(0, 4);
			string monthpart = token.Substring(5, 2);
			string daypart = token.Substring(8, 2);
			string hourspart = token.Substring(11, 2);
			string minutespart = token.Substring(14, 2);

			int year = Convert.ToInt32(yearpart);
			int month = Convert.ToInt32(monthpart);
			int day = Convert.ToInt32(daypart);
			int hours = Convert.ToInt32(hourspart);
			int minutes = Convert.ToInt32(minutespart);

			DateTime value = new DateTime(year, month, day, hours, minutes, 0);
			return value;
		}


		#endregion
	}


}
﻿using System.ComponentModel;
using System.Windows.Forms;



namespace LIS_BASIC
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuFileSave = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuFileNew = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panelTop = new System.Windows.Forms.Panel();
			this.etCode2 = new LIS_BASIC.NumberedTextBox();
			this.panelBottom = new System.Windows.Forms.Panel();
			this.etSayComment = new System.Windows.Forms.TextBox();
			this.stError = new System.Windows.Forms.Label();
			this.etErrorMessage = new System.Windows.Forms.TextBox();
			this.etResult = new System.Windows.Forms.TextBox();
			this.pbRun = new System.Windows.Forms.Button();
			this.menuStrip1.SuspendLayout();
			this.panelTop.SuspendLayout();
			this.panelBottom.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1006, 27);
			this.menuStrip1.TabIndex = 5;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.reloadToolStripMenuItem,
            this.toolStripSeparator1,
            this.mnuFileSave,
            this.mnuFileSaveAs,
            this.toolStripSeparator3,
            this.mnuFileNew,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 23);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
			this.loadToolStripMenuItem.Text = "&Open";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// reloadToolStripMenuItem
			// 
			this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
			this.reloadToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
			this.reloadToolStripMenuItem.Text = "&Reload";
			this.reloadToolStripMenuItem.Click += new System.EventHandler(this.reloadToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(131, 6);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Size = new System.Drawing.Size(134, 24);
			this.mnuFileSave.Text = "&Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveAs
			// 
			this.mnuFileSaveAs.Name = "mnuFileSaveAs";
			this.mnuFileSaveAs.Size = new System.Drawing.Size(134, 24);
			this.mnuFileSaveAs.Text = "Save &As...";
			this.mnuFileSaveAs.Click += new System.EventHandler(this.mnuFileSaveAs_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(131, 6);
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Name = "mnuFileNew";
			this.mnuFileNew.Size = new System.Drawing.Size(134, 24);
			this.mnuFileNew.Text = "&New";
			this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(131, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// panelTop
			// 
			this.panelTop.Controls.Add(this.etCode2);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTop.Location = new System.Drawing.Point(0, 27);
			this.panelTop.Margin = new System.Windows.Forms.Padding(0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Padding = new System.Windows.Forms.Padding(6);
			this.panelTop.Size = new System.Drawing.Size(1006, 390);
			this.panelTop.TabIndex = 6;
			// 
			// etCode2
			// 
			this.etCode2.BackColor = System.Drawing.Color.DarkGray;
			this.etCode2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.etCode2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.etCode2.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.etCode2.LineNumberColor = System.Drawing.Color.Maroon;
			this.etCode2.Location = new System.Drawing.Point(6, 6);
			this.etCode2.Name = "etCode2";
			this.etCode2.Padding = new System.Windows.Forms.Padding(3);
			this.etCode2.Size = new System.Drawing.Size(994, 378);
			this.etCode2.TabIndex = 5;
			this.etCode2.Load += new System.EventHandler(this.etCode2_Load);
			// 
			// panelBottom
			// 
			this.panelBottom.Controls.Add(this.etSayComment);
			this.panelBottom.Controls.Add(this.stError);
			this.panelBottom.Controls.Add(this.etErrorMessage);
			this.panelBottom.Controls.Add(this.etResult);
			this.panelBottom.Controls.Add(this.pbRun);
			this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelBottom.Location = new System.Drawing.Point(0, 417);
			this.panelBottom.Name = "panelBottom";
			this.panelBottom.Size = new System.Drawing.Size(1006, 171);
			this.panelBottom.TabIndex = 7;
			// 
			// etSayComment
			// 
			this.etSayComment.BackColor = System.Drawing.Color.White;
			this.etSayComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.etSayComment.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.etSayComment.ForeColor = System.Drawing.Color.Black;
			this.etSayComment.Location = new System.Drawing.Point(6, 39);
			this.etSayComment.Multiline = true;
			this.etSayComment.Name = "etSayComment";
			this.etSayComment.Size = new System.Drawing.Size(462, 126);
			this.etSayComment.TabIndex = 8;
			// 
			// stError
			// 
			this.stError.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.stError.ForeColor = System.Drawing.Color.Red;
			this.stError.Location = new System.Drawing.Point(838, 10);
			this.stError.Name = "stError";
			this.stError.Size = new System.Drawing.Size(48, 20);
			this.stError.TabIndex = 7;
			this.stError.Text = "ERROR";
			// 
			// etErrorMessage
			// 
			this.etErrorMessage.BackColor = System.Drawing.Color.White;
			this.etErrorMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.etErrorMessage.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.etErrorMessage.ForeColor = System.Drawing.Color.Black;
			this.etErrorMessage.Location = new System.Drawing.Point(474, 39);
			this.etErrorMessage.Multiline = true;
			this.etErrorMessage.Name = "etErrorMessage";
			this.etErrorMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.etErrorMessage.Size = new System.Drawing.Size(526, 126);
			this.etErrorMessage.TabIndex = 6;
			// 
			// etResult
			// 
			this.etResult.BackColor = System.Drawing.Color.White;
			this.etResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.etResult.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.etResult.ForeColor = System.Drawing.Color.Black;
			this.etResult.Location = new System.Drawing.Point(6, 8);
			this.etResult.Name = "etResult";
			this.etResult.Size = new System.Drawing.Size(462, 22);
			this.etResult.TabIndex = 5;
			// 
			// pbRun
			// 
			this.pbRun.Font = new System.Drawing.Font("Line Printer", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pbRun.Location = new System.Drawing.Point(892, 5);
			this.pbRun.Name = "pbRun";
			this.pbRun.Size = new System.Drawing.Size(106, 28);
			this.pbRun.TabIndex = 4;
			this.pbRun.Text = "Run";
			this.pbRun.UseVisualStyleBackColor = true;
			this.pbRun.Click += new System.EventHandler(this.pbRun_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1006, 588);
			this.Controls.Add(this.panelTop);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.panelBottom);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "LIS Calculated Results Tester";
			this.Load += new System.EventHandler(this._Form1_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.panelTop.ResumeLayout(false);
			this.panelBottom.ResumeLayout(false);
			this.panelBottom.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private MenuStrip menuStrip1;
		private ToolStripMenuItem fileToolStripMenuItem;
		private ToolStripMenuItem loadToolStripMenuItem;
		private ToolStripMenuItem reloadToolStripMenuItem;
		private ToolStripSeparator toolStripSeparator1;
		private ToolStripMenuItem exitToolStripMenuItem;
		private Panel panelTop;
		private NumberedTextBox etCode2;
		private Panel panelBottom;
		private TextBox etErrorMessage;
		private TextBox etResult;
		private Button pbRun;
		private ToolStripMenuItem mnuFileSave;
		private ToolStripSeparator toolStripSeparator2;
		private ToolStripMenuItem mnuFileSaveAs;
		private Label stError;
		private TextBox etSayComment;
		private ToolStripSeparator toolStripSeparator3;
		private ToolStripMenuItem mnuFileNew;
	}
}


﻿using System;



namespace LIS_BASIC
{
	public class SimpleUnaryExpression : SimpleExpression
	{
		private readonly SimpleExpression _operand;
		private readonly string _operator;

		public SimpleUnaryExpression(Context context, SimpleExpression operand, string op)
			:base(context)
		{
			_operand = operand;
			_operator = op;
		}

		public override object Value
		{
			get
			{
				if (_operator.ToUpper() == "NOT")
					return !Convert.ToBoolean(_operand.Value);
				else
					return -Convert.ToDouble(_operand.Value); 
			}
		}

	}
}

﻿namespace LIS_BASIC
{
	public class FunctionTextIn : SimpleExpression
	{
		private readonly string _stringLiteral;
		private readonly SimpleExpression _expr;

		public FunctionTextIn(Context context, SimpleExpression expr, string stringliteral)
			: base(context)
		{
			_expr = expr;
			_stringLiteral = stringliteral;
		}

		public override object Value
		{
			get
			{
				string sExpr = _expr.Value.ToString();
				if (sExpr.Contains(_stringLiteral))
					return true;
				return false;
			}
		}
	}
}

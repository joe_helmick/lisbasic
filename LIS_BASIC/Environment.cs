﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;

namespace LIS_BASIC
{
	public static class BasicEnvironment
	{
		public static Dictionary<string, string> CommentsRepository = new Dictionary<string, string>();


		public static void LoadStockComments()
		{
			// Load the context with all the stock comments.
			// Load all the calculation comments -- from an XML file for the time being.
			string CommentsXMLFileName = Application.StartupPath + @"\..\..\..\CalculationComments.xml";
			XDocument xDoc = XDocument.Load(CommentsXMLFileName);
			var root = xDoc.Element("CalculationComments");
			var comments = root.Elements("Comment");
			foreach (var comment in comments)
			{
				string key = comment.Attribute("name").Value;
				string value = comment.Attribute("text").Value;
				CommentsRepository.Add(key, value);
			}
		}


	}
}

﻿namespace LIS_BASIC
{
	public class SimpleAssignmentStatement : SimpleStatement
	{
		private readonly string _mName;
		private readonly SimpleExpression _mAssignValue;

		public SimpleAssignmentStatement(Context context,
			string name, SimpleExpression assignValue)
			: base(context)
		{
			_mName = name.ToUpper();
			_mAssignValue = assignValue;
		}

		public override void Execute()
		{
			Context.Variables[_mName] = _mAssignValue.Value;
		}
	}
}

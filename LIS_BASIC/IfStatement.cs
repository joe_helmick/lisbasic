﻿using System;



namespace LIS_BASIC
{
	public class SimpleIfStatement : SimpleStatement
	{
		private readonly SimpleExpression _mIfClause;
		private readonly SimpleStatement _mThenClause;
		private readonly SimpleStatement _mElseClause;

		public SimpleIfStatement(Context context, SimpleExpression ifClause,
			SimpleStatement thenClause, SimpleStatement elseClause)
			: base(context)
		{
			_mIfClause = ifClause;
			_mThenClause = thenClause;
			_mElseClause = elseClause;
		}

		public override void Execute()
		{
			if (Convert.ToString(_mIfClause.Value) == true.ToString())
			{
				_mThenClause.Execute();
			}
			else
			{
				if (_mElseClause != null)
				{
					_mElseClause.Execute();
				}
			}
		}
	}
}

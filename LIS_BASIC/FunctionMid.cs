﻿using System;



namespace LIS_BASIC
{
	public class FunctionMid : SimpleExpression
	{
		private readonly SimpleExpression _text;
		private readonly int _start;
		private readonly int _chars;

		public FunctionMid(Context context, SimpleExpression text, int start, int chars)
			: base(context)
		{
			_text = text;
			_start = start;
			_chars = chars;
		}

		public override object Value
		{

			get
			{
				try
				{
					string target = _text.Value.ToString();

					if (_chars >= target.Length)
						return target;
					else
						return target.Substring(_start, _chars);
				}
				catch
				{
					throw new ArgumentOutOfRangeException("start, length", "ERROR: One or both parameters is wrong or negative.");
				}


			}
		}

	}
}

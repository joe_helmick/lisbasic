﻿namespace LIS_BASIC
{
    public class SimpleBool : SimpleExpression
    {
        private readonly bool _mValue;

        public SimpleBool(Context context, bool value)
            : base(context)
        {
            _mValue = value;
        }

        public override object Value
        {
            get { return _mValue; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIS_BASIC
{
	public class DecimalsStatement : SimpleStatement
	{
		private readonly int _count;

		public DecimalsStatement(Context context, int count)
			: base(context)
		{
			_count = count;
		}

		public override void Execute()
		{
			Context.Decimals = _count;
		}
	}
}

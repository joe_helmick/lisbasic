﻿using System;
using System.Runtime.Serialization;



namespace LIS_BASIC
{
	[Serializable()]
	public class SymbolException : Exception
	{
		public SymbolException(string message)
			: base(message)
		{
		}

		public SymbolException(string message,
			Exception inner)
			: base(message, inner)
		{
		}

		protected SymbolException(SerializationInfo info,
			StreamingContext context)
			: base(info, context)
		{
		}

	}

	[Serializable()]
	public class RuleException : Exception
	{

		public RuleException(string message)
			: base(message)
		{
		}

		public RuleException(string message,
							 Exception inner)
			: base(message, inner)
		{
		}

		protected RuleException(SerializationInfo info,
								StreamingContext context)
			: base(info, context)
		{
		}

	}



}

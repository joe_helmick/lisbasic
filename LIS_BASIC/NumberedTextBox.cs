﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LIS_BASIC
{
	// REFERENCE http://stackoverflow.com/questions/124975/windows-forms-textbox-that-has-line-numbers

	public partial class NumberedTextBox : UserControl
	{
		private int _lines;

		[Browsable(true),
			EditorAttribute("System.ComponentModel.Design.MultilineStringEditor, System.Design", "System.Drawing.Design.UITypeEditor")]
		new public String Text
		{
			get
			{
				return editBox.Text;
			}
			set
			{
				editBox.Text = value;
				Invalidate();
			}
		}

		private Color _lineNumberColor = Color.Black;

		[Browsable(true), DefaultValue(typeof(Color), "Black")]
		public Color LineNumberColor
		{
			get
			{
				return _lineNumberColor;
			}
			set
			{
				_lineNumberColor = value;
				Invalidate();
			}
		}

		[Browsable(true)]
		[DefaultValue(0)]
		public int SelectionStart
		{
			get { return editBox.SelectionStart; }
			set { editBox.SelectionStart = value; }
		}


		public NumberedTextBox()
		{
			InitializeComponent();

			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
			SetStyle(ControlStyles.ResizeRedraw, true);
			editBox.SelectionChanged += SelectionChanged;
			editBox.VScroll += OnVScroll;
		}

		private void SelectionChanged(object sender, EventArgs args)
		{
			Invalidate();
		}

		private void DrawLines(Graphics g)
		{
			g.Clear(BackColor);
			int y = -editBox.ScrollPos.Y;
			for (int i = 1; i < _lines + 1; i++)
			{
//				SizeF size = g.MeasureString(i.ToString(), Font);
				g.DrawString(i.ToString(), Font, new SolidBrush(LineNumberColor), new Point(3, y));
				y += Font.Height -1;
			}
			int max = (int)g.MeasureString((_lines + 1).ToString(), Font).Width + 6;
			editBox.Location = new Point(max, 0);
			editBox.Size = new Size(ClientRectangle.Width - max, ClientRectangle.Height);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			_lines = editBox.Lines.Count();
			DrawLines(e.Graphics);
			e.Graphics.TranslateTransform(50, 0);
			editBox.Invalidate();
			base.OnPaint(e);
		}

		private void OnVScroll(object sender, EventArgs e)
		{
			Invalidate();
		}

		public void Select(int start, int length)
		{
			editBox.Select(start, length);
		}

		public void ScrollToCaret()
		{
			editBox.ScrollToCaret();
		}

		private void editBox_TextChanged(object sender, EventArgs e)
		{
			Invalidate();
		}
	}

	public class RichTextBoxEx : RichTextBox
	{
		private double _yfactor = 1.0d;

		[DllImport("user32.dll")]
		static extern IntPtr SendMessage(IntPtr hWnd, Int32 wMsg, Int32 wParam, ref Point lParam);

		private enum WindowsMessages
		{
			wm_user = 0x400,
			em_getscrollpos = wm_user + 221,
			em_setscrollpos = wm_user + 222
		}

		public Point ScrollPos
		{
			get
			{
				Point scrollPoint = new Point();
				SendMessage(Handle, (int)WindowsMessages.em_getscrollpos, 0, ref scrollPoint);
				return scrollPoint;
			}
			set
			{
				Point original = value;
				if (original.Y < 0)
					original.Y = 0;
				if (original.X < 0)
					original.X = 0;

				Point factored = value;
				factored.Y = (int)(original.Y * _yfactor);

				Point result = value;

				SendMessage(Handle, (int)WindowsMessages.em_setscrollpos, 0, ref factored);
				SendMessage(Handle, (int)WindowsMessages.em_getscrollpos, 0, ref result);

				int loopcount = 0;
				const int MAXLOOP = 100;
				while (result.Y != original.Y)
				{
					// Adjust the input.
					if (result.Y > original.Y)
						factored.Y -= (result.Y - original.Y) / 2 - 1;
					else if (result.Y < original.Y)
						factored.Y += (original.Y - result.Y) / 2 + 1;

					// test the new input.
					SendMessage(Handle, (int)WindowsMessages.em_setscrollpos, 0, ref factored);
					SendMessage(Handle, (int)WindowsMessages.em_getscrollpos, 0, ref result);

					// save new factor, test for exit.
					loopcount++;
					if (loopcount >= MAXLOOP || result.Y == original.Y)
					{
						_yfactor = factored.Y / (double)original.Y;
						break;
					}
				}
			}
		}
	}
}

﻿using System;
using System.CodeDom;
using System.Text;



namespace LIS_BASIC
{
	public class SayStatement : SimpleStatement
	{
		private readonly SimpleExpression _displayClause;
		private readonly string _literalPrefix;
		private readonly string _literalSuffix;

		public SayStatement(Context context, string literalPrefix, SimpleExpression displayClause, string literalSuffix)
			: base(context)
		{
			_literalPrefix = literalPrefix;
			if (literalPrefix != null)
				_literalPrefix = literalPrefix.Replace("\"", "");

			_literalSuffix = literalSuffix;
			if (_literalSuffix != null)
				_literalSuffix = literalSuffix.Replace("\"", "");

			_displayClause = displayClause;
		}

		public override void Execute()
		{
			try
			{
				double dValue;
				bool isDouble = Double.TryParse(_displayClause.Value.ToString(), out dValue);
				if (isDouble && Context.Decimals != - 1)
				{
					StringBuilder sbFmt = new StringBuilder();
					sbFmt.Append("0.");
					for (int n = 0; n < Context.Decimals; n++)
						sbFmt.Append("0");


					dValue = Math.Round(dValue, Context.Decimals);
					Context.SayValue = _literalPrefix + dValue.ToString(sbFmt.ToString()) + _literalSuffix;
				}
				else
					Context.SayValue = _literalPrefix + _displayClause.Value + _literalSuffix;
			}
			catch (SymbolException symex)
			{
				throw symex;
			}

			catch (ArgumentOutOfRangeException)
			{
				throw;
			}
			catch (Exception)
			{
				SimpleId id = _displayClause as SimpleId;
				if (id != null)
				{
					SimpleId s = id;
					throw new SymbolException("Cannot make sense of the symbol: " + s.Name + ".  Did you misspell a variable or test name?");
				}
			}
		}
	}
}

﻿using System;
using System.Diagnostics;



namespace LIS_BASIC
{
	public class SimpleBinaryExpression : SimpleExpression
	{
		private readonly SimpleExpression _leftOperand;
		private readonly string _operator;
		private readonly SimpleExpression _rightOperand;

		public SimpleBinaryExpression(Context context,
			SimpleExpression leftOperand, string op, SimpleExpression rightOperand)
			: base(context)
		{
			_leftOperand = leftOperand;
			_operator = op;
			_rightOperand = rightOperand;
		}

		public override object Value
		{
			get
			{
				object lValue = _leftOperand.Value;
				object rValue = _rightOperand.Value;


				try
				{
					switch (_operator.ToUpper())
					{
						case "AND":
							return Convert.ToBoolean(lValue) && Convert.ToBoolean(rValue);

						case "OR":
							return Convert.ToBoolean(lValue) || Convert.ToBoolean(rValue);

						case "+":
							return Convert.ToDouble(lValue) + Convert.ToDouble(rValue);

						case "-":
							return Convert.ToDouble(lValue) - Convert.ToDouble(rValue);

						case "&":
							return Convert.ToString(lValue) + Convert.ToString(rValue);

						case "*":
							return Convert.ToDouble(lValue) * Convert.ToDouble(rValue);

						case "/":
							return Convert.ToDouble(lValue) / Convert.ToDouble(rValue);

						case ">":
							return (Convert.ToDouble(lValue) > Convert.ToDouble(rValue)).ToString();

						case "<":
							return (Convert.ToDouble(lValue) < Convert.ToDouble(rValue)).ToString();

						case "<=":
							return (Convert.ToDouble(lValue) <= Convert.ToDouble(rValue)).ToString();

						case ">=":
							return (Convert.ToDouble(lValue) >= Convert.ToDouble(rValue)).ToString();

						case "^":
							{
								double term = Convert.ToDouble(lValue);
								double power = Convert.ToDouble(rValue);
								return Math.Pow(term, power);
							}

						case "==":
							if (lValue is string && rValue is string)
							{
								string sL = lValue.ToString().Replace("\"", "");
								string sR = rValue.ToString().Replace("\"", "");
								return (String.Compare(sL, sR, true) == 0).ToString(); //== Compare strings
							}
							else
							{
								return (Convert.ToDouble(lValue) == Convert.ToDouble(rValue)).ToString(); //== Compare values
							}

						case "<>":
							if (lValue.GetType() == typeof(string) && rValue.GetType() == typeof(string))
							{
								string sL = lValue.ToString().Replace("\"", "");
								string sR = rValue.ToString().Replace("\"", "");

								return (String.Compare(sL, sR, true) != 0).ToString(); //== Compare strings
							}
							else
							{
								return (Convert.ToDouble(lValue) != Convert.ToDouble(rValue)).ToString(); //== Compare values
							}
					}
					return null;

				}
				catch (Exception)
				{
					string explanation = string.Format("A math or comparison operation was supplied the wrong term type, so could not complete.  The terms are {0} {2} {1}.", lValue, rValue, _operator);
					throw new ArgumentException(explanation);
				}

			}
		}
	}
}

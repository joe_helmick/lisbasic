﻿

namespace LIS_BASIC
{
	public enum SymbolConstants 
	{
		symbol_eof = 0, // (EOF)
		symbol_error = 1, // (Error)
		symbol_comment = 2, // Comment
		symbol_newline = 3, // NewLine
		symbol_whitespace = 4, // Whitespace
		symbol_divdiv = 5, // '//'
		symbol_minus = 6, // '-'
		symbol_ampersand = 7,
		symbol_lparen = 8, // '('
		symbol_rparen = 9, // ')'
		symbol_times = 10, // '*'
		symbol_comma = 11, // ','
		symbol_div = 12, // '/'
		symbol_caret = 13, // '^'
		symbol_plus = 14, // '+'
		symbol_lt = 15, // '<'
		symbol_lteq = 16, // '<='
		symbol_ltgt = 17, // '<>'
		symbol_eq = 18, // '='
		symbol_eqeq = 19, // '=='
		symbol_gt = 20, // '>'
		symbol_gteq = 21, // '>='
		symbol_and = 22, // and
		symbol_cancel = 23, // CANCEL
		symbol_dateliteral = 24, // DateLiteral
		symbol_datetimeliteral = 25, // DateTimeLiteral
		symbol_daysdiff = 26,
		symbol_decimals = 27,
		symbol_else = 28, // ELSE
		symbol_end = 29, // END
		symbol_halt = 30,
		symbol_hoursdiff = 31, // HOURS
		symbol_id = 32, // Id
		symbol_if = 33, // IF
		symbol_intliteral = 34, // IntLiteral
		symbol_left = 35, // LEFT
		symbol_let = 36, // LET
		symbol_log = 37,
		symbol_mid = 38, // MID
		symbol_minutesdiff = 39, // MINUTES
		symbol_not = 40, // NOT
		symbol_number = 41,
		symbol_or = 42, // NOT
        symbol_orderhas = 43, //ORDERHAS
		symbol_realliteral = 44, // RealLiteral
		symbol_right = 45, // RIGHT
		symbol_say = 46, // SAY
		symbol_saycomment = 47,
		symbol_stringliteral = 48, // StringLiteral
		symbol_textin = 49, // TEXTIN
		symbol_then = 50, // THEN
		symbol_yearsdiff = 51
	};

	public enum RuleConstants 
	{
		rule_function = 0, // <Function> ::= <TestName> <Arguments> <Stmts>
		rule_testname_id = 1, // <TestName> ::= Id
		rule_arguments_lparen_rparen = 2, // <Arguments> ::= '(' <Args> ')'
		rule_arguments_lparen_rparen2 = 3, // <Arguments> ::= '(' ')'
		rule_arg_id_eq_realliteral = 4, // <Arg> ::= Id '=' RealLiteral
		rule_arg_id_eq_stringliteral = 5, // <Arg> ::= Id '=' StringLiteral
		rule_arg_id_eq_dateliteral = 6, // <Arg> ::= Id '=' DateLiteral
		rule_arg_id_eq_datetimeliteral = 7, // <Arg> ::= Id '=' DateTimeLiteral
		rule_arg_id_eq_intliteral = 8, // <Arg> ::= Id '=' IntLiteral
		rule_stmts = 9, // <Stmts> ::= <Stmt> <Stmts>
		rule_stmts2 = 10, // <Stmts> ::= <Stmt>
		rule_args_comma = 11, // <Args> ::= <Arg> ',' <Args>
		rule_args = 12, // <Args> ::= <Arg>
		rule_constant_stringliteral = 13, // <Constant> ::= StringLiteral
		rule_constant_realliteral = 14, // <Constant> ::= RealLiteral
		rule_constant_dateliteral = 15, // <Constant> ::= DateLiteral
		rule_constant_datetimeliteral = 16, // <Constant> ::= DateTimeLiteral
		rule_constant_intliteral = 17, // <Constant> ::= IntLiteral
		rule_expr_or = 18, // <Expr> ::= <Expr> or <Equ>
		rule_expr_and = 19, // <Expr> ::= <Expr> and <Equ>
		rule_expr = 20, // <Expr> ::= <Equ>
		rule_equ_eqeq = 21, // <Equ> ::= <Equ> '==' <Comp>
		rule_equ_ltgt = 22, // <Equ> ::= <Equ> '<>' <Comp>
		rule_equ = 23, // <Equ> ::= <Comp>
		rule_comp_gt = 24, // <Comp> ::= <Comp> '>' <Add>
		rule_comp_lt = 25, // <Comp> ::= <Comp> '<' <Add>
		rule_comp_gteq = 26, // <Comp> ::= <Comp> '>=' <Add>
		rule_comp_lteq = 27, // <Comp> ::= <Comp> '<=' <Add>
		rule_comp = 28, // <Comp> ::= <Add>
		rule_add_plus = 29, // <Add> ::= <Add> '+' <Mult>
		rule_add_minus = 30, // <Add> ::= <Add> '-' <Mult>
		rule_concat = 31,  // <Add> ::= <Add> '&' <Mult>
		rule_add = 32, // <Add> ::= <Mult>
		rule_mult_times = 33, // <Mult> ::= <Mult> '*' <Pow>
		rule_mult_div = 34, // <Mult> ::= <Mult> '/' <Pow>
		rule_mult = 35, // <Mult> ::= <Pow>
		rule_pow_caret = 36, // <Pow> ::= <Pow> '^' <Unary>
		rule_pow = 37, // <Pow> ::= <Unary>
		rule_unary_minus = 38, // <Unary> ::= '-' <Value>
		rule_unary_not = 39, // <Unary> ::= NOT <Value>
		rule_unary = 40, // <Unary> ::= <Value>
		rule_ifstmt_if_then_end = 41, // <IfStmt> ::= IF <Expr> THEN <Stmts> END
		rule_ifstmt_if_then_else_end = 42, // <IfStmt> ::= IF <Expr> THEN <Stmts> ELSE <Stmts> END
		rule_saystmt_say_expr = 43, // <SayStmt> ::= SAY <Expr> 
		rule_saystmt_say_sl_expr = 44, // <SayStmt> ::= SAY StringLiteral , <Expr> 
		rule_saystmt_say_sl_expr_sl = 45, // <SayStmt> ::= SAY StringLiteral , <Expr> , StringLiteral
		rule_cancelstmt = 46, // <CancelStmt> ::= CANCEL 
		rule_cancelstmt_id = 47, // <CancelStmt> ::= CANCEL Id 
		rule_assignstmt = 48, // <AssignStmt> ::= LET Id '=' <Expr> 
		rule_commentstmt = 49, // <CommentStmt> ::= SAYCOMMENT Id
		rule_haltstmt = 50, // <HaltStmt> ::= HALT
		rule_decimals = 51, // <DecimalsStmt> ::= DECIMALS IntLiteral
		rule_stmt = 52, // <Stmt> ::= <IfStmt>
		rule_stmt2 = 53, // <Stmt> ::= <AssignStmt>
		rule_stmt3 = 54, // <Stmt> ::= <CancelStmt>
		rule_stmt4 = 55, // <Stmt> ::= <SayStmt>
		rule_stmt5 = 56, // <Stmt> ::= <CommentStmt>
		rule_stmt6 = 57, // <Stmt> ::= <HaltStmt>
		rule_stmt7 = 58, // <Stmt> ::= <DecimalsStmt>
		rule_value_lparen_rparen = 59, // <Value> ::= '(' <Expr> ')'
		rule_value_id = 60, // <Value> ::= Id
		rule_value = 61, // <Value> ::= <Constant>
		rule_value2 = 62, // <Value> ::= <BoolFunc>
		rule_value3 = 63, // <Value> ::= <RealFunc>
		rule_value4 = 64, // <Value> ::= <StringFunc>
		rule_boolfunc_textin = 65, // <BoolFunc> ::= TEXTIN '(' <Expr> ',' StringLiteral ')'
        rule_boolfunc_orderhas = 66,
		rule_realfunc_minutesdiff = 67, 
		rule_realfunc_hoursdiff = 68, 
		rule_realfunc_daysdiff = 69, 
		rule_realfunc_yearsdiff = 70,
		rule_realfunc_number = 71,
		rule_realfunc_log = 72,
		rule_stringfunc_left = 73, // <StringFunc> ::= LEFT '(' <Expr> ',' IntLiteral ')'
		rule_stringfunc_right = 74, // <StringFunc> ::= RIGHT '(' <Expr> ',' IntLiteral ')'
		rule_stringfunc_mid = 75,  // <StringFunc> ::= MID '(' <Expr> ',' IntLiteral ',' IntLiteral ')'
	
	};

}

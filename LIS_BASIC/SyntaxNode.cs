﻿namespace LIS_BASIC
{
	
	public class SimpleSyntaxNode
	{
		private readonly Context _context;

		public SimpleSyntaxNode(Context context)
		{
			_context = context;
		}

		public Context Context
		{
			get { return _context; }
		}
	}
	
	
	

}

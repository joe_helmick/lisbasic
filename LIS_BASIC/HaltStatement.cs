﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LIS_BASIC
{
	class HaltStatement : SimpleStatement
	{
		public HaltStatement(Context context)
			: base(context)
		{
		}


		public override void Execute()
		{
			Context.ExecutionHalted = true;
		}

	}
}

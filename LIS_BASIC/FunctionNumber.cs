﻿using System;
using System.Text;



namespace LIS_BASIC
{
	public class FunctionNumber : SimpleExpression
	{
		private readonly SimpleExpression _text;

		public FunctionNumber(Context context, SimpleExpression text)
			: base(context)
		{
			_text = text;
		}


		public override object Value
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				char[] textArray = _text.Value.ToString().ToCharArray();
				foreach (char c in textArray)
				{
					switch (c)
					{
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
						case '.':
						case '+':
						case '-':
							sb.Append(c);
							break;
					}
				}

				double d = Convert.ToDouble(sb.ToString());

				if (Context.Decimals != -1)
				{
					d = Math.Round(d, Context.Decimals);
				}

				return d;



				// LINQ more compact but way slower. JLH
				//StringBuilder sb = new StringBuilder();
				//string text = _text.Value.ToString();

				//foreach (char c in text.Where(c => c == '.' ||  c == '+' || c == '-' || Char.IsDigit(c)))
				//{
				//	sb.Append(c);
				//}

			}
		}
	}
}

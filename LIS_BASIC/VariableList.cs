﻿using System.Collections.Generic;



namespace LIS_BASIC
{


	public class VariableList
	{
		private readonly Dictionary <string, object> _varList = new Dictionary <string, object>();

		public bool Add(string name, object value)
		{
			string upper = name.ToUpper();
			if (_varList.ContainsKey(upper))
				return false;
			_varList.Add(upper, value);
			return true;
		}


		public void ClearValues()
		{
			_varList.Clear();
		}


		public int Count
		{
			get
			{
				return _varList.Count;
			}
		}

		public object this[string name]
		{
			get
			{
				return _varList[name];
			}
			set
			{
				_varList[name] = value;
			}
		}

		private class Variable
		{
			private readonly string _name;
			private object _value;


			public Variable(string name, object value)
			{
				_name = name;
				_value = value;
			}
		}
	}


}
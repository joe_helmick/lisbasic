﻿namespace LIS_BASIC
{
	public class SimpleReal : SimpleExpression
	{
		private readonly double _mValue;

		public SimpleReal(Context context, double value)
			: base(context)
		{
			_mValue = value;
		}

		public override object Value
		{
			get { return _mValue; }
		}
	}
}

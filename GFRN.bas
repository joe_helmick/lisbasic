// GFRN     GFR NKF              $C:=POW(@CREAT,-1.154); $A:=POW(@AGE,-0.203); $G:=186*$C*$A; MATCH($@SEX,"F") ? (MATCH($@RACE,"B") ? (0.742 * 1.21 * $G) : (0.742 * $G)) : (MATCH($@RACE,"B") ? (1.21 * $G) : $G)

@GFRN (@NOW = 2017-04-22, @CREAT=1.0, @DOB=1964-02-04, @SEX= "M", @RACE ="B")

let @AGE = YEARSDIFF(@NOW, @DOB)
let @C_ = @CREAT ^ -1.154
let @A_ = @AGE ^ -0.203
let @G_ = 186.0 * @C_ * @A_

if @SEX == "F"
THEN
	IF @RACE == "B"
	THEN
		SAY 0.742 * 1.21 * @G_
	ELSE
		SAY 0.742 * @G_
	END
ELSE
	IF @RACE == "B"
	THEN
		SAY 1.21 * @G_
	ELSE
		SAY @G_
	END
END

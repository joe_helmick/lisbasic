// DPU4     D/P Urea at 4 Hr     $R:=(@UREA4/@BUN);$R>=0.975? $R + " @HTR":($R>0.905&&$R<0.975? $R + "@HATR":($R>=0.835&&$R<=0.905? $R + " @LATR":($R<0.835? $R + " @LTR":$R)))
 
@DPU4 ( @UREA4 = 0.903, @BUN = 1.0 ) 

LET @R = @UREA4/@BUN

IF @R >= 0.975 
THEN
	say @R
	saycomment @HTR
ELSE
	IF @R > 0.90 AND @R < 0.975
	THEN
		say @R
		saycomment @HATR
	ELSE
		IF @R >= 0.835 AND @R <=0.905

// there's a bug here, an overlap between 0.900 and 0.905

		THEN
			SAY @R
			saycomment @LATR
		ELSE
			IF @R < 0.835
			THEN
				SAY @R
				saycomment @LTR
			ELSE
				SAY @R
			END
		END
	END
END

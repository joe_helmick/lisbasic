//URTPC    Total Protein/CreatinMATCH("<*",$@URTP) && MATCH("<*",$@UCRER)?"N/A":(MATCH("<*",$@URTP)?"<" + (@URTP/@UCRER)*1000:(MATCH("<*",$@UCRER)?">" + (@URTP/@UCRER)*1000: (@URTP/@UCRER)*1000))

@URTPC ( @URTP="<1.0" , @UCRER=1.0 )
LET @TERM_ = (NUMBER(@URTP) / NUMBER(@UCRER)) * 1000.0

IF TEXTIN(@URTP, "<") AND TEXTIN(@UCRER, "<")
then
	say "N/A"
else
	if TEXTIN(@URTP, "<")
	then
		say "<" & @TERM_
	else
		if TEXTIN(@UCRER, "<*")
		then 
			say ">" & @TERM_
		else
			say @TERM_
		end
	end
end

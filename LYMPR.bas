//LYMPR $A:=(@LUC<=6.0?@LUC+@LYMPA:@LYMPA,@LYMPA);$M:=(@LUC<6.1?@LUC+@LYMPM:@LYMPM,@LYMPM);ANY(@CBCXD)?".":(MATCH("PERFORMED",$@MDIFP)?$M:$A,$A)

@LYMPR ( @LUC = 5.0, @LYMPA = 4.0, @LYMPM = 3.0, @MDIFP="PERFORMED" )

if @LUC <= 6.0
then
    let @A = @LUC+@LYMPA
else
    let @A = @LYMPA
end

if @LUC < 6.1
then 
    let @M = @LUC+@LYMPM
else
    let @M = @LYMPM
end

if orderhas(@CBCXD) 
then
    cancel
else
    if textin(@MDIFP, "PERFORMED")
    then
        say @M
    else
        say @A
    end
end

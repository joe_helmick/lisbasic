//V   DBFLD("ACCDATE","AGE")<5843?(0.135*POW(@HT,0.535)* POW(@WT,0.666))+" @PV": (DBFLD("","SEX")=="M"?(-14.012934+ 0.296785*@WT+0.1947*@HT):(-35.270121+0.183809*@WT+0.344547*@HT))

@V ( @DDATE=2017-04-21, @DOB=1964-02-24, @WT=100, @HT=185, @GENDER="M", @PV = 1)

IF YEARSDIFF(@DDATE, @DOB) < 16.0
then
	say 0.135 * @HT^0.535 * @WT ^ 0.666
else
	if @GENDER == "M"
	then
		say -14.012934 + (0.296785 * @WT) + (0.1947 * @HT)
	else
		say -35.270121 + (0.183809 * @WT) + (0.344547 * @HT)
	end
end



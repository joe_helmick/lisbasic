��
03/31/17                                                    Page 1

                    Calculated Individual Tests

-----------------------------------------------------------------------------
 ID       NAME                 CALCULATION
-----------------------------------------------------------------------------
 A1CON    HGB A1C Sort         ANY(@HGB)?(((MATCH("*",$@HGB))||(MATCH(".",(DBFLD("HGB","ARESULT")))))?-9999.0:$@HGB): $@HGB
 ADJCA    Calcium, Adj. Total  @CA >= 4.0?(@ALB<4.0?0.8*(4.0-@ALB)+@CA:@CA) : "N/A"
 ADJCP    CA*PO4, Adjusted     @CA >= 4.0 && @PHOS >= 1.0?(@ADJCA*@PHOS) : "N/A"
 AGAP     Anion Gap            MESEXPDATE("AGAP",GETDBF("ACCDATE"));<<AGAP>>
 AGRAT    A/G Ratio            (@TP-@ALB)>0?(@ALB/@GLOB):"N/A"
 BASOR    Basophils            ANY(@CBCXD)?".":(MATCH("PERFORMED",$@MDIFP)?$@BASOM:$@BASOA),$@BASOA
 BSA      Body Surface Area    DBFLD("ACCDATE","AGE") < 5843? (0.024268*(POW(@HT,0.3964)* POW(@WT,0.5378)))+ " @PBSA":(0.007184 * POW(@WT,0.425) * POW(@HT,0.725))
 CBCAD    Bill CBC Automated Di(MATCH("PERFORMED",$@MDIFP))?-9999.0:@CBCAD,@CBCAD
 CCEX1    Corr Creat, PD ExchanMESEXPDATE("CCEX1",GETDBF("AODATE"));<<CCEX1>>,@CCEX1
 CCEX2    Corr Creat, PD ExchanMESEXPDATE("CCEX2",GETDBF("AODATE"));<<CCEX2>>,@CCEX2
 CCEX3    Corr Creat, PD ExchanMESEXPDATE("CCEX3",GETDBF("AODATE"));<<CCEX3>>,@CCEX3
 CCEX4    Corr Creat, PD ExchanMESEXPDATE("CCEX4",GETDBF("AODATE"));<<CCEX4>>,@CCEX4
 CCEX5    Corr Creat, PD ExchanMESEXPDATE("CCEX5",GETDBF("AODATE"));<<CCEX5>>,@CCEX5
 CCQA     Corr Creat, PD QA    MESEXPDATE("CCQA",GETDBF("AODATE"));<<CCQA>>,@CCQA
 CCR      Weekly Total CrCl    @GFR+(((@CCR24/@CREAT)*(@DVOL/1000)*7))*1.73/@BSA
 CCR0     Corr Creat, 0 Hr DwelMESEXPDATE("CCR0",GETDBF("AODATE"));<<CCR0>>,@CCR0
 CCR0M    Corr Creat, 0 Hr DwelMESEXPDATE("CCR0M",GETDBF("AODATE"));<<CCR0M>>,@CCR0M
 CCR1M    Corr Creat, 1 Hr DwelMESEXPDATE("CCR1M",GETDBF("AODATE"));<<CCR1M>>,@CCR1M
 CCR2     Corr Creat, 2 Hr DwelMESEXPDATE("CCR2",GETDBF("AODATE"));<<CCR2>>,@CCR2
 CCR24    Corr Creat, 24 Hr DiaMATCH("<*",$@CR24)?("<"+@CR24):(MESEXPDATE("CCR24",GETDBF("AODATE"));<<CCR24>>,@CCR24)
 CCR2M    Corr Creat, 2 Hr DwelMESEXPDATE("CCR2M",GETDBF("AODATE"));<<CCR2M>>,@CCR2M
 CCR4     Corr Creat, 4 Hr DwelMESEXPDATE("CCR4",GETDBF("AODATE"));<<CCR4>>,@CCR4
 CCR4F    Corr Creat, 4 Hr DwelMESEXPDATE("CCR4F",GETDBF("AODATE"));<<CCR4F>>,@CCR4F
 CCR4M    Corr Creat, 4 Hr DwelMESEXPDATE("CCR4M",GETDBF("AODATE"));<<CCR4M>>,@CCR4M
 CCREF    Corr. Creatinine, FluMESEXPDATE("CCREF",GETDBF("AODATE"));<<CCREF>>,@CCREF
 CCRON    Corr Creat, OvernightMESEXPDATE("CCRON",GETDBF("AODATE"));<<CCRON>>,@CCRON
 CHRA     Cholesterol/HDL Ratio@CHOL/@HDL
 CP       CA*PO4               @CA >= 4.0 && @PHOS >= 1.0?(@CA*@PHOS) : "N/A"
 CRCC     Creatinine Clearance (@UCRER/@CREAT)*(@UVOL/@CTIME)*(1.73/@BSA)
 CRCR     Residual Renal Creati(@UCRER/@CREAT)*(@UVOL/@CTIME)*10.08
 CRCRB    Weekly Residual CrCl ANY(@UUNR) ? (@UCRER/@CREAT*@UVOL/@CTIME*10.08)*1.73/@BSA : ((ANY(@PDADT) || @UVOL == 0) ? (@V*0) : ".")
 CREAT    Creatinine           MESEXPDATE("CREAT",GETDBF("AODATE"));<<CREAT>>,@CREAT
 CRR      Creat. Reduction Rati@CREAP > 0.0 && @CREAT > 0.0 && @CREAP < @CREAT? (1-(@CREAP/@CREAT))*100 : "@PGP"
 CTIME    Collection Time      $J:=DATEN($@SDATE); $K:=@STIME; $L:=DATEN($@EDATE); $M:=@ETIME; <<CALTT>>
 CTRFX    Colony Count ID      @CT1>49?"@CTID":-9999.0
 DATEV    Date Validation      DATEV($@SDATE)
 DCAR     Calcium, Dialysate   MATCH("<*",$@DCA1)?"<"+@DCA1/2:(MATCH(">*",$@DCA1)?">"+@DCA1/2:(@DCA1/2))
 DCR      Weekly Dialysate CrClMATCH("<*",$@CCR24)?("<"+@CCR24):((@CCR24/@CREAT)*(@DVOL/1000)*7)*1.73/@BSA
 DD0G2    D/D0 Glucose at 2 Hr @GLU2/@GLU0
 DD0G4    D/D0 Glucose at 4 Hr $R:=(@GLU4/@GLU0);$R<0.255? $R + " @HTR":($R>=0.255&&$R<0.375? $R + " @HATR":($R>=0.375&&$R<0.4855? $R + " @LATR":($R>=0.4855? $R + " @LTR":$R)))
 DKTV     Weekly Dialysate Kt/V((@UN24/@BUN)*(@DVOL/1000)*7)/@V
 DMGR     Magnesium, Dialysate MATCH("<*",$@DMG1)?"<"+@DMG1/1.2:(MATCH(">*",$@DMG1)?">"+@DMG1/1.2:(@DMG1/1.2))
 DPC0     D/P Corr Creat at 0 H@CCR0/@CREAT
 DPC2     D/P Corr Creat at 2 H@CCR2/@CREAT
 DPC4     D/P Corr Creat at 4 H$R:=(@CCR4/@CREAT);$R>0.805? $R + " @HTR":($R>0.645&&$R<=0.805? $R + "@HATR":($R>=0.495&&$R<=0.645? $R + " @LATR":($R<0.495? $R + " @LTR":$R)))
 DPC4F    D/P Corr Creat at 4 H$R:=(@CCR4F/@CREAT);$R>0.805? $R + " @HTR":($R>0.645&&$R<=0.805? $R + " @HATR":($R>=0.495&&$R<=0.645? $R + " @LATR":($R<0.495? $R + " @LTR":$R)))
 DPC4M    D/P Corr Creat at 4 H@CCR4M/@CREAT
 DPCC0    D/P Corr Creat at 0 H@CCR0/@CRB2
 DPCC2    D/P Corr Creat at 2 H@CCR2/@CRB2
 DPCC4    D/P Corr Creat at 4 H$R:=(@CCR4/@CRB2);$R>0.805? $R + " @HTR":($R>0.645&&$R<=0.805? $R + " @HATR":($R>=0.495&&$R<=0.645? $R + " @LATR":($R<0.495? $R + " @LTR":$R)))
 DPN0     D/P Sodium at 0 Hr   @NA0M/@NA
 DPN1     D/P Sodium at 1 Hr   @NA1M/@NA


03/31/17                                                    Page 2

                    Calculated Individual Tests

-----------------------------------------------------------------------------
 ID       NAME                 CALCULATION
-----------------------------------------------------------------------------
 DPN2     D/P Sodium at 2 Hr   @NA2M/@NA
 DPN4     D/P Sodium at 4 Hr   @NA4M/@NA
 DPU0     D/P Urea at 0 Hr     @UREA0/@BUN
 DPU2     D/P Urea at 2 Hr     @UREA2/@BUN
 DPU4     D/P Urea at 4 Hr     $R:=(@UREA4/@BUN);$R>=0.975? $R + " @HTR":($R>0.905&&$R<0.975? $R + "@HATR":($R>=0.835&&$R<=0.905? $R + " @LATR":($R<0.835? $R + " @LTR":$R)))
 DPU4F    D/P Urea at 4 Hr     $R:=(@UREA4/@BUN);$R>=0.975? $R + " @HTR":($R>0.905&&$R<0.975? $R + " @HATR":($R>=0.835&&$R<=0.905? $R + " @LATR":($R<0.835? $R + " @LTR":$R)))
 DPUR0    D/P Urea at 0 Hr     @UREA0/@BUNB2
 DPUR2    D/P Urea at 2 Hr     @UREA2/@BUNB2
 DPUR4    D/P Urea at 4 Hr     $R:=(@UREA4/@BUNB2);$R>=0.975? $R + " @HTR":($R>0.905&&$R<0.975? $R + " @HATR":($R>=0.835&&$R<=0.905? $R + " @LATR":($R<0.835? $R + " @LTR":$R)))
 DRB1     Bill Drug 1          ANY($@DRGSR)?"BILLING":"."
 EDATE    End Date             STRSTR($@EDATE,"/")?DATEV($@EDATE):DATEV($@EDATE[0,2]+"/"+$@EDATE[2,2]+"/"+$@EDATE[4,6])
 EOSR     Eosinophils          ANY(@CBCXD)?".":(MATCH("PERFORMED",$@MDIFP)?$@EOSM:$@EOSA),$@EOSA
 GFR      Weekly Residual GFR  ANY(@UUNR) ? (@CRCR+@KRUR)/2*1.73/@BSA : ((ANY(@PDADT) || @UVOL == 0) ? (@V*0) : ".")
 GFRA     GFR African American $C:=POW(@CREAT,-1.154);$A:=POW(((DBFLD("","AGE"))/365.25),-0.203);$G:=175*$C*$A;(DBFLD("","SEX")=="F")?(($G>66.829)?(">60"):(0.742*1.21*$G)):(($G>49.587)?(">60"):(1.21*$G))
 GFRN     GFR NKF              $C:=POW(@CREAT,-1.154); $A:=POW(@AGE,-0.203); $G:=186*$C*$A; MATCH($@SEX,"F") ? (MATCH($@RACE,"B") ? (0.742 * 1.21 * $G) : (0.742 * $G)) : (MATCH($@RACE,"B") ? (1.21 * $G) : $G)
 GFRN1    GFR nonAfrican Americ$C:=POW(@CREAT,-1.154);$A:=POW(((DBFLD("","AGE"))/365.25),-0.203);$G:=175*$C*$A;(DBFLD("","SEX")=="F")?(($G>80.863)?(">60"):(0.742*$G)):(($G>60)?(">60"):($G))
 GLOB     Globulin             (@TP-@ALB)>0?(@TP-@ALB):"N/A"
 HBCMR    Hep B Core Ab, IgM ReMATCH("Positive",$@HBCT)?"Performed":"."
 HBSC     Hep B Surface Ag ConfMATCH("Confirmed",$@HBCOF)?"BILLING":"."
 HCRPT    Hep C Patient State  MATCH("Reactive",$@HCV1)||MATCH("Equivocal@HCVE",$@HCV1)?((DBFLD("","STATE")=="NY")?"NY":((DBFLD("","STATE")=="")? "":"nonNY")):"."
 HCTB     Bill Only HCT        @HCT > 0 ? "BILLING" : "."
 HGBB     Bill Only Hemoglobin @HGB>0 ? "BILLING" : "."
 HGBP3    HGB, Post x 3        @HGBP*3
 HGBX3    HGB x 3              @HGB*3
 HHR      HCT HGB Ratio        @HCT/@HGB
 HIVPT    HIV Patient State    MATCH("Reflexed",$@HIVAB)?((DBFLD("","STATE")=="NY")?"NY":((DBFLD("","STATE")=="")? "":"nonNY")):"."
 KRUR     Resid Renal Urea Clea(@UUNR/@BUN)*(@UVOL/@CTIME)*10.08
 KTV      Kt/V Natural Log     MESEXPDATE("KTV",GETDBF("ACCDATE"));<<KTV>>,@KTV
 KTVE     Kt/V Equilibrated    MESEXPDATE("KTVE",GETDBF("ACCDATE"));<<KTVE>>,@KTVE
 KTVJ     Kt/V Jindal CalculatiMESEXPDATE("KTVJ",GETDBF("ACCDATE"));<<KTVJ>>,@KTVJ
 KTVP     Weekly Total Kt/V    DBFLD("","ACCDATE")>20100201?(@DKTV  + @RKTV):$@KTVP
 KTVS     Kt/V Standard        MESEXPDATE("KTVS",GETDBF("ACCDATE"));<<KTVS>>,@KTVS
 LDLC     LDL Cholesterol Calc.$X:=@CHOL-(@VLDL+@HDL);@TRIG>400?"@NLDL":($X<0?"0":$X)
 LYMPR    Lymphocytes          $A:=(@LUC<=6.0?@LUC+@LYMPA:@LYMPA,@LYMPA);$M:=(@LUC<6.1?@LUC+@LYMPM:@LYMPM,@LYMPM);ANY(@CBCXD)?".":(MATCH("PERFORMED",$@MDIFP)?$M:$A,$A)
 LYMPT    Total Manual Lymphs fMATCH("PERFORMED",$@MDIFP)?(@LYMPM,0)+(@ATLYM,0)+(@RLYMP,0)+(@PROLY,0):$@LYMPT,$@LYMPT
 MDIFP    Manual Differential  ANY((@SEGM,@BAND,@LYMPM,@MONOM,@EOSM,@BASOM,@META,@ATLYM,@MYELO,@BLAST,@PROMY))?"PERFORMED":$@MDIFP,$@MDIFP
 MONOR    Monocytes            ANY(@CBCXD)?".":(MATCH("PERFORMED",$@MDIFP)?$@MONOM:$@MONOA),$@MONOA
 MONOT    Total Manual Monos foMATCH("PERFORMED",$@MDIFP)?(@MONOM,0) + (@PROMN,0):$@MONOT,$@MONOT
 NPNA     nPNA, Peritoneal Dial(10.76*((0.69*@UNA)+1.46))/(@V/0.58)
 NPNAH    nPNA, Hemodialysis   @TXNUM == 1 ? @BUN/(36.3+5.48*@KTV+53.5/@KTV)+0.168:(@TXNUM == 2 ? @BUN/(25.8+1.15*@KTV+56.4/@KTV)+0.168:(@TXNUM == 3 ? @BUN/(16.3+4.3*@KTV+56.6/@KTV)+0.168:"."))
 NRBCR    Nucleated RBC        @NRBC<5?-9999.0:@NRBC
 NYHC1    Holding Time Comment @NYHT1>1440?"See Below@NYHC":"."
 NYHC2    Holding Time Comment @NYHT2>1440?"See Below@NYHC":"."
 NYHC3    Holding Time Comment @NYHT3>1440?"See Below@NYHC":"."
 NYHC4    Holding Time Comment @NYHT4>2880?"See Below@NYHC":"."
 NYHC5    Holding Time Comment @NYHT5>1440?"See Below@NYHC":"."
 NYHT1    Holding Time         $J:=DATEN($@NYDAT); $K:=@NYTIM-300; $L:=DATEN($@NYDT1); $M:=@NYAT1; <<CALTT>>
 NYHT2    Holding Time         $J:=DATEN($@NYDAT); $K:=@NYTIM-300; $L:=DATEN($@NYDT2); $M:=@NYAT2; <<CALTT>>
 NYHT3    Holding Time         $J:=DATEN($@NYDAT); $K:=@NYTIM-300; $L:=DATEN($@NYDT3); $M:=@NYAT3; <<CALTT>>
 NYHT4    Holding Time         $J:=DATEN($@NYDAT); $K:=@NYTIM-300; $L:=DATEN($@NYDT4); $M:=@NYAT4; <<CALTT>>
 NYHT5    Holding Time         $J:=DATEN($@NYDAT); $K:=@NYTIM-300; $L:=DATEN($@NYDT5); $M:=@NYAT5; <<CALTT>>
 NYNO3    Nitrate (as N)       @NYNIT<0.1?"<0.02":@NYNIT*0.226


03/31/17                                                    Page 3

                    Calculated Individual Tests

-----------------------------------------------------------------------------
 ID       NAME                 CALCULATION
-----------------------------------------------------------------------------
 NYNOD    Nitrate (as N)       @NYNTD<0.1?"<0.02":@NYNTD*0.226
 PNA      PNA                  10.76*((0.69*@UNA)+1.46)
 RBCB     Bill Only RBC        @RBC > 0 ? "BILLING" : "."
 RECIR    Recirculation        (@BUN_P>0 && @BUN_V>0 && @BUN_A>0) ? (@BUN_P<=@BUN_V ? "@RSV" :(@BUN_P-@BUN_A)/(@BUN_P-@BUN_V)*100) :"."
 RKTV     Weekly Residual Kt/V ANY(@UUNR) ? (@KRUR/@V) : ((ANY(@PDADT) || @UVOL == 0) ? (@V*0) : ".")
 SAT      Transferrin SaturatioMATCH("<*",$@IRON) && MATCH("<*",$@TRSF)?"@UTC":(MATCH("<*",$@IRON)?"<" + (@IRON*100)/(@TRSF*1.4):(MATCH("<*",$@TRSF)?">" + (@IRON*100)/(@TRSF*1.4):(@IRON*100)/(@TRSF*1.4)))
 SDATE    Start Date           STRSTR($@SDATE,"/")?DATEV($@SDATE):DATEV($@SDATE[0,2]+"/"+$@SDATE[2,2]+"/"+$@SDATE[4,6])
 SEGR     Neutrophils          ANY(@CBCXD)?".": (MATCH("PERFORMED",$@MDIFP)?$@SEGM:$@SEGA),$@SEGA
 SEGT     Total Manual Segs forMATCH("PERFORMED",$@MDIFP)?(@SEGM,0)+(@BAND,0)+(@META,0)+(@MYELO,0)+(@PROMY,0)+(@MYEBL,0)+(@BLAST):$@SEGT,$@SEGT
 TIBC     TIBC, Calculated     MATCH ("<*",$@TRSF)?"<"+@TRSF*1.4:@TRSF*1.4
 TPQ      T Pro, PD QA         @TPQA <4 ? "<0.004" : @TPQA/1000
 TPX1     T Pro, PD Exchange 1 @TPEX1 <4 ? "<0.004" : @TPEX1/1000
 TPX2     T Pro, PD Exchange 2 @TPEX2 <4 ? "<0.004" : @TPEX2/1000
 TPX3     T Pro, PD Exchange 3 @TPEX3 <4 ? "<0.004" : @TPEX3/1000
 TPX4     T Pro, PD Exchange 4 @TPEX4 <4 ? "<0.004" : @TPEX4/1000
 TPX5     T Pro, PD Exchange 5 @TPEX5 <4 ? "<0.004" : @TPEX5/1000
 UAAMB    UA-Automated MicroscoMATCH ("Performed @UAMC", $@UAMC)? "BILLED" : -9999.0
 UAATB    UA-Automated Billing MATCH ("Not Indicated", $@UAMC)? "BILLED" : -9999.0
 UCREC    24 Hr Urine Creat    (!ANY(@CTIME)||@CTIME==1440)?((@UCRER*@UVOL)/100): ((@UCRER*@UVOL)/100)*(1440/@CTIME)
 UKRU     KRU-Res. Urea Clearan(@UUNR*@UVOL)/(@BUN*0.9*@CTIME)
 UNA      UNA                  ANY(@UUNR) ? (@DVOL*@UN24)/100000+(((@UVOL*@UUNR)/100000)/@CTIME)*1440 : ((ANY(@PDADT) || @UVOL == 0) ? ((@DVOL*@UN24)/100000) : ".")
 UPRO     Urine Protein, 24 Hr @URTP <4 ? "N/A" : (@URTP/100)*((@UVOL)/(@CTIME/1440))
 UPRO1    Urine Protein, 24 Hr @URTP<4?"N/A":((@URTP/100)*((@UVOL)/(@CTIME/1440)))/(((@UCRER/100)*(@UVOL/(@CTIME/1440)))/1000)
 URR      Urea Reduction Ratio (@BUNP>0&&@BUN>0)?(@BUNP<@BUN?(@BUNP==2.0?(1-(@BUNP/@BUN))*100+"@PBL": (MATCH("<*",$@BUNP)?">"+(1-(@BUNP/@BUN))*100+"@PBL":(1-(@BUNP/@BUN))*100)): "@PGP1"):"."
 URRT     Urea Reduction Ratio @BUNT >= 1.0 &&  @BUN > 0 && @BUNT < @BUN? (1-(@BUNT/@BUN))*100 : "@PGP"
 URTPC    Total Protein/CreatinMATCH("<*",$@URTP) && MATCH("<*",$@UCRER)?"N/A":(MATCH("<*",$@URTP)?"<" + (@URTP/@UCRER)*1000:(MATCH("<*",$@UCRER)?">" + (@URTP/@UCRER)*1000: (@URTP/@UCRER)*1000))
 UTPCR    Urine Creatinine, 24 ((@UCRER/100)*(@UVOL/(@CTIME/1440)))/1000
 UUNC     24 Hr Ur Urea Nitroge(!ANY(@CTIME)||@CTIME==1440)?((@UUNR*@UVOL)/100000):((@UUNR*@UVOL)/100000)* (1440/@CTIME)
 V        Total Body Water     DBFLD("ACCDATE","AGE")<5843?(0.135*POW(@HT,0.535)* POW(@WT,0.666))+" @PV": (DBFLD("","SEX")=="M"?(-14.012934+ 0.296785*@WT+0.1947*@HT):(-35.270121+0.183809*@WT+0.344547*@HT))
 VLDL     VLDL Cholesterol     @TRIG/5
 WBCC     WBC Corrected for NRB(@WBCX*100)/(@NRBC+100)
 WBCCR    WBC Corrected for NRB@NRBC<5?-9999.0:@WBCC
 WBCIR    WBC                  @NRBC<5?@WBCI:-9999.0,@WBCI
 WBCX     1235-WBCX            (@NRBC*(100/@LIMIT))>4?$@WBCC:$@WBCI,$@WBCI
 WNIN     Nitrate (as N)       @WNIT < 1.0 ? "<0.2" : @WNIT * 0.226
 ZZCA     Dummy CA Experiment  @CA>0? "." : "."
 ZZORD    Active Order         (@BUN>0||@HGB>0||@CA>0||@K>0)? -9999.0 : "."

��
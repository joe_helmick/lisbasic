//UUNC     24 Hr Ur Urea Nitroge(!ANY(@CTIME)||@CTIME==1440)?((@UUNR*@UVOL)/100000):((@UUNR*@UVOL)/100000)* (1440/@CTIME)

@UUNC (@RESDATE=2017-04-25@15:55, @COLDATE=2017-04-24@14:24, @UUNR = 300.0, @UVOL = 400.0)
LET @X_ = @UUNR * @UVOL / 100000.0

// MINUTESDIFF(@LATERDATE, @EARLIERDATE) gives a positive number of minutes.
LET @MINUTES_ = MINUTESDIFF(@RESDATE, @COLDATE)

// MINUTESDIFF(@LATERDATE, @EARLIERDATE) gives a positive number.

// JLH if I'm reading the formula correctly, it doesn't make sense.
// Almost never will the result be _EXACTLY_ 1440 minutes later, right?
// So the formula will almost always take the second branch. Might as well
// simplify it and just use the second branch all the time.  Am I
// missing something?

IF @MINUTES_ == 1440
THEN
	say @X_
ELSE
	say @X_ * 1440 / @MINUTES_
END



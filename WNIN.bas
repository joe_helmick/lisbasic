//WNIN     Nitrate (as N)       @WNIT < 1.0 ? "<0.2" : @WNIT * 0.226

@WNIN (@WNIT = 0.6)
if @WNIT < 1.0
then
	say "<0.2"
else
	say @WNIT * 0.226
end

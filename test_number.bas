// JLH to test further, experiment with varying values of @A

@ABC (@A = "<0.008")
LET @A_ = NUMBER(@A)
IF @A_ < 0.01 THEN SAY "SMALL" ELSE SAY "LARGER" END

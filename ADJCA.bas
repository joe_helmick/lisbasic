//ADJCA    Calcium, Adj. Total  @CA >= 4.0?(@ALB<4.0?0.8*(4.0-@ALB)+@CA:@CA) : "N/A"

@ADJCA (@CA = 7.0, @ALB = 3.0)

if @CA >= 4.0
then
	if @ALB < 4.0
	then
		say 0.8 * (4.0 - @ALB) + @CA
	else
		say @CA
	end
else
	say "N/A"
end
